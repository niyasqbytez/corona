<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>{{ env('APP_NAME') }} Password reset</title>
		<meta id="viewport" name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
		<meta http-equiv="Content-Type" content="text/html"/>
		<link rel="shortcut icon" type="image/png" href="images/icons/fav.png"/>
		
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('css/components.min.css') }}" />

		<!-- <link rel="stylesheet" type="text/css" href="public/global/css/plugins.min.css" /> -->

		<link rel="stylesheet" type="text/css" href="{{ asset('css/darkblue.min.css') }}" />

		<link rel="stylesheet" type="text/css" href="{{ asset('plugins/global/uniform/css/uniform.default.min.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('plugins/dist/css/AdminLTE.min.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('css/login/login-soft.css') }}" />
		
		<!--[if IE]>
		<link href="ie.css" rel="stylesheet" type="text/css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body class="login" style="height: auto;" cz-shortcut-listen="true">
		<div class="logo">
			<a href="">
				<img src="{{ asset('images/erp_logo.png') }}" alt="{{ env('APP_NAME') }}">
			</a>
		</div>
			
			
		<div class="content">
			<!-- BEGIN RESET FORM -->
			<form class="" id="" method="POST" action="{{ route('admin.password.email') }}">
				{{ csrf_field() }}
				<h3>Forget Password ?</h3>
				<p>
					Enter your e-mail address below to reset your password.
				</p>
				<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
					<div class="input-icon">
						<i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="email" autocomplete="off" placeholder="E-Mail Address" name="email" value="{{ old('email') }}" autofocus="">
                        @if($errors->has('email'))<span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>@endif
					</div>
				</div>
				<div class="form-actions">
					<a class="btn btn-primary backBtn" id="" href="{{ route('admin.login') }}"> <i class="fa fa-angle-left"></i> Back</a>
					<button type="submit" class="btn blue pull-right">Submit <i class="fa fa-arrow-circle-right"></i></button>
				</div>
			</form>
			<!-- END RESET FORM -->
		</div>
			
		<!-- END CONTENT -->
		
		<div class="copyright">
			{{ date('Y') }} Copyright © QBYTEZ Infolabs
		</div>
		
		<script>
			
		</script>
			


		<script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.checkbox').on('click', function() {
					if($('.checker span').hasClass('checked')) {
						$('.checker span').removeClass('checked');
						return false;
					} else {
						$('.checker span').addClass('checked');
						return false;
					}
				})
			})
		</script>
		<script type="text/javascript" src="{{ asset('plugins/global/backstretch/jquery.backstretch.min.js') }}"></script>
	</body>
</html>
<script>
	$(document).ready(function () {
		// init background slide images
		$.backstretch([
				"{{ asset('layouts/login_page/1.jpg') }}"
			], {
				fade: 1000,
				duration: 4000
			}
		);
	});
</script>