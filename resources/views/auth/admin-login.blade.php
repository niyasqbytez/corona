<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>{{ env('APP_NAME') }} Login</title>
		<meta id="viewport" name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
		<meta http-equiv="Content-Type" content="text/html"/>
    	<link rel="shortcut icon" type="image/png" href="{{ asset('images/icons/fav.png') }}"/>
		
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('css/components.min.css') }}" />

		<link rel="stylesheet" type="text/css" href="{{ asset('css/darkblue.min.css') }}" />

		<link rel="stylesheet" type="text/css" href="{{ asset('plugins/global/uniform/css/uniform.default.min.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('plugins/dist/css/AdminLTE.min.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ asset('css/login/login-soft.css') }}" />
		
		<!--[if IE]>
		<link href="ie.css" rel="stylesheet" type="text/css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body class="login" style="height: auto;" cz-shortcut-listen="true">
		<div class="logo">
			<a href="">
				<img src="{{ asset('images/erp_logo.png') }}" alt="{{ env('APP_NAME') }}">
			</a>
		</div>
		<div class="content">
			<!-- BEGIN LOGIN FORM -->
			<form method="POST" id="login_form" class="login-form" action="{{ route('admin.login') }}">
				{{ csrf_field() }}
				@if($errors->has('password') == false && $errors->has('email') == false)
					@include('admin.includes.msg')   
				@endif  
				<h3 class="form-title">Login to your accountsss</h3>
		
				<div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
					<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
					<label class="control-label visible-ie8 visible-ie9">Username</label>
					<div class="input-icon">
						<i class="fa fa-user"></i>
						<input id="username" type="text" class="form-control placeholder-no-fix" placeholder="Username" name="email" value="{{ old('email') }}">
						@if($errors->has('email'))<span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>@endif
					</div>
				</div>
				<div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
					<label class="control-label visible-ie8 visible-ie9">Password</label>
					<div class="input-icon">
						<i class="fa fa-lock"></i>
						<input id="password" type="password" placeholder="Password" class="form-control placeholder-no-fix" name="password">
						@if($errors->has('password'))<span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>@endif
					</div>
				</div>
				<div class="form-actions">
					<label class="checkbox">
						<div class="checker">
							<span class="">
								<input type="checkbox" name="remember">
							</span>
						</div> Remember Me
					</label>
					<button type="submit" class="btn blue pull-right login_btn">Login <i class="fa fa-arrow-circle-right"></i></button>
				</div>

				<div class="forget-password">
					<h4>Forgot your password ?</h4>
					<p>
						no worries, click <a href="{{ route('admin.password.request') }}">here</a>
						to reset your password.
					</p>
				</div>
			</form>
			<!-- END LOGIN FORM -->
		</div>
			
		<!-- END CONTENT -->
		
		<div class="copyright">
			{{ date('Y') }} Copyright © QBYTEZ Infolabs
		</div>
		
		<script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/myadmin.js') }}"></script>
		<script type="text/javascript" src="{{ asset('plugins/global/backstretch/jquery.backstretch.min.js') }}"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.checkbox').on('click', function() {
					if($('.checker span').hasClass('checked')) {
						$('.checker span').removeClass('checked');
						return false;
					} else {
						$('.checker span').addClass('checked');
						return false;
					}
				})
				$.backstretch([
					// init background slide images
					"{{ asset('layouts/login_page/8.jpg') }}"
					], {
						fade: 1000,
						duration: 4000
					}
				);
				@if (Session::has('growl'))
					@if (is_array(Session::get('growl')))
						growl("{!! Session::get('growl')[0] !!}", "{{ Session::get('growl')[1] }}");
					@else
						growl("{{Session::get('growl')}}");
					@endif
				@endif
			})
		</script>
	</body>
</html>