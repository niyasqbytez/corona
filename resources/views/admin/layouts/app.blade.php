<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

    @include('admin.includes.head')
    @yield('styles')
    <style>
        .dataTable thead th {
            border: none !important;
        }
    </style>

</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-sidebar-closed">

        <div id="busyloading" class="loding-wrap" style="display: none;">
            <div class="loader">
                <div class="top_status_bar load">
                    <div class="progress_bar bar"></div>
                </div>
            </div>
        </div>

        <!-- CONTAINER WRAP -->
        <div class="page-container" id="page-container">
			<!-- BEGIN HEADER INNER -->
			@include('admin.includes.header')
			<!-- END HEADER INNER -->
            <!-- //CRM SIDEBAR -->
            @include('admin.includes.sidebar')
            <!-- END CRM SIDEBAR -->

            <!-- CRM CONTENT BODY -->
            <div class="page-content-wrapper">
                @yield('content')
            </div>
            <!-- END CRM CONTENT BODY -->
        </div>
		<!-- END CONTAINER WRAP -->
		<!-- BEGIN BOTTOM INNER -->
		<div class="page-footer">
			<div class="page-footer-inner"> {{ date('Y') }} © QBYTEZ Infolabs</div>
			<div class="scroll-to-top" style="display: block;">
				<i class="icon-arrow-up"></i>
			</div>
		</div>
		<!-- END BOTTOM INNER -->

        <!--FOR SHOWING MESSAGE AFTER AJAX START-->
        <div class="success_box"></div>
        <div class="alert_box_general"></div>
        <div class="warning_box_general"></div>
        <div class="info_box_general"></div>
        <!--FOR SHOWING MESSAGE AFTER AJAX START-->

        <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('plugins/global/bootstrap-toastr/toastr.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('public/global/select2/js/select2.full.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/custom-scripts.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('plugins/global/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>


        <script type="text/javascript">
            //Alert Message Danger/Success
            function alertbox(type, msg, title) {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                if (type == 'danger') {
                    if (title == undefined) {
                        title = 'Error !';
                    }
                    toastr.error(msg, title)
                } else if (type == 'success') {
                    if (title == undefined) {
                        title = 'Success !';
                    }
                    toastr.success(msg, title)
                } else if (type == 'warning') {
                    if (title == undefined) {
                        title = 'Warning !';
                    }
                    toastr.warning(msg, title)
                } else if (type == 'info') {
                    if (title == undefined) {
                        title = 'Info !';
                    }
                    toastr.info(msg, title)
                }
            }
            $(".dangerBtn").on("click", function () {
                alertbox('danger', 'Connection/Database error occured contact with admin!');
            });
            $(".infoBtn").on("click", function () {
                alertbox('info', 'Connection/Database error occured contact with admin!');
            });
            $(".warningBtn").on("click", function () {
                alertbox('Warning', 'Connection/Database error occured contact with admin!');
            });
            $(".successBtn").on("click", function () {
                alertbox('success', 'Connection/Database error occured contact with admin!');
            });

        </script>
        @yield('admin_js')
    </body>

</html>
