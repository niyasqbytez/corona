<!-- BEGIN HEADER INNER -->
<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="">
                <img src="{{asset('images/logo.png')}}" alt="logo" class="logo-default"> </a>
            <div class="menu-toggler sidebar-toggler"></div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">

            <ul class="nav navbar-nav pull-right">
                <!---SET LANGUAGE -->
                <!--MENU BUTTON TOGGLE-->
                <li class="dropdown dropdown-user">
                    <span class="menu_bar_toggle">
                        <i class="fa fa-plus"></i>
                    </span>
                </li>
                <!-- <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                    <a href="javascript:;" class="dropdown-toggle" id="notificationscount">
                        <i class="fa fa-bell"></i>
                        <span class="badge badge-default"> 0 </span>
                    </a>
                </li> -->
                <!--MENU BUTTON TOGGLE-->
                <div class="dropdown-menu_custom box" style="">
                    <div class="noti_head">
                        <div class="not_title">Notifications <span class="not_count">0</span></div>
                        <div class="view_all_link"><a href=""><i class="fa fa-arrow-up"></i></a></div>
                    </div>
                    <div class="notifications">
                        <div class="each_notification">No notifications found.</div>
                    </div>
                </div>
                <!-- END NOTIFICATION DROPDOWN -->
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="" class="dropdown-toggle user_options" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="images/avatar2.png" />
                        <span class="username username-hide-on-mobile">{{ Auth::guard('admin')->user()->email }}</span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-quick-sidebar-toggler">
                    <a href="" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                        class="logout" title="Logout">
                        <i class="fa fa-sign-out"></i>
                    </a>
                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
</div>
<!-- END HEADER INNER -->





<!-- TOP-BAR USER DTS -->
<div class="user_drop_menu box" style="display: none;">
	<span class="each_option_drop delegate_role">Delegate Role</span>
	<a href="" class="each_option_drop switch_user" style="color: #000;">Profile</a>
</div>
<!-- END TOP-BAR USER-PRO -->
<!-- TOP-BAR USER-DTS -->
<div class="nav_bar_menu box" style="display: none;">
	<div class="inner_wrap">
		<span class="title">CONTACTS</span>
		<div class="menu_row">
			<div class="each_menu">
				<a href="">
					<img class="each_menu_img" src="{{ asset('public/global/img/topbar_icons/lead.png') }}">
					<span>Account</span>
				</a>
			</div>
			<div class="each_menu">
				<a href="">
					<img class="each_menu_img" src="{{ asset('public/global/img/topbar_icons/customer.png') }}">
					<span>Contact</span>
				</a>
			</div>
			<div class="each_menu">
				<a href="">
					<img class="each_menu_img" src="{{ asset('public/global/img/topbar_icons/supplier.png') }}">
					<span>Employee</span>
				</a>
			</div>
		</div>
	</div>
	<div class="inner_wrap">
		<span class="title">SALES ACTIVITIES</span>
		<div class="menu_row">
			<div class="each_menu">
				<a href="">
					<img class="each_menu_img" src="{{ asset('public/global/img/topbar_icons/lead.png') }}">
					<span>Lead</span>
				</a>
			</div>
			<div class="each_menu">
				<a href="">
					<img class="each_menu_img" src="{{ asset('public/global/img/topbar_icons/customer.png') }}">
					<span>Opportunity</span>
				</a>
			</div>
		</div>
	</div>
	<div class="inner_wrap">
		<span class="title">TASK &amp; ACTIVITIES</span>
		<div class="menu_row">
			<div class="each_menu">
				<a href="" class="global_add_new_task">
					<img class="each_menu_img" src="{{ asset('public/global/img/topbar_icons/quotation.png') }}">
					<span>Task</span>
				</a>
			</div>
			<div class="each_menu hide">
				<a href="">
					<img class="each_menu_img" src="{{ asset('public/global/img/topbar_icons/quotation.png') }}">
					<span>Note</span>
				</a>
			</div>
			<div class="each_menu">
				<a href="" class="add_new_quote_general">
					<img class="each_menu_img" src="{{ asset('public/global/img/topbar_icons/quotation.png') }}">
					<span>Quote</span>
				</a>
			</div>
		</div>
	</div>
	
</div>
<!-- END TOP-BAR USER-DTS -->
<!-- TOP-BAR USER DTS -->
<div class="user_drop_premises box" style="display: none;">
	<a href="" class="each_option_drop switch_user">Nostalgia Dubai Mall</a>
	<a href="" class="each_option_drop switch_user">Nostalgia Dubai Mall</a>
	<a href="" class="each_option_drop switch_user">Nostalgia Dubai Mall</a>
	<a href="" class="each_option_drop switch_user">Nostalgia Dubai Mall</a>
</div>
<!-- END TOP-BAR USER-PRO -->