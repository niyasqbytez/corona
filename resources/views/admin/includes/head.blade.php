<head>
    <meta charset="utf-8" />
    <title>{{ env('APP_NAME') }} @yield('title')</title>
    <meta id="viewport" name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
    <meta http-equiv="Content-Type" content="text/html"/>
    <!-- <link rel="shortcut icon" type="image/png" href="images/icons/fav.png"/> -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/components.min.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ asset('public/global/layout/css/layout.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/darkblue.min.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ asset('public/global/select2/css/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('public/global/select2/css/select2-bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/global/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/global/bootstrap-toastr/toastr.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom-style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}" />
    
    <style>
        #language-switch {
            margin-right: 30px;
            margin-top: 8px;
            color: #ffffff;
            background-color: #333;
            border: 1px solid #333 !important;
            width: 200px;
            padding: 5px;
            border-radius: 5px;
            outline: none;
        }
    </style>
    
    <!--[if IE]>
    <link href="ie.css" rel="stylesheet" type="text/css" />
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
