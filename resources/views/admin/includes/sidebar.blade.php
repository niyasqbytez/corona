<!-- //CRM SIDEBAR -->
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<!-- CRM SIDEBAR MENU -->
		<ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
			<li class="sidebar-toggler-wrapper hide">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler"></div>
				<!-- END SIDEBAR TOGGLER BUTTON -->
			</li>

			<!--******************************************** Campaigns ********************************************-->
			<li class="nav-item start {{ Request::is('admin*') ? 'active' : ''}}">
				<a href="{{ route('admin.dashboard') }}" class="nav-link nav-toggle">
					<img src="{{ asset('images/sidebar_icons/campaigns.png') }}" class="sidebar_icons">
					<span class="title">Dashboard</span>
				</a>
			</li>

			<!--******************************************** Content List ********************************************-->
			{{-- <li class="nav-item start parent_li {{ Request::is('admin/content-list*') ? 'active' : ''}}">
				<a href="{{ route('admin.content-list.index') }}" class="nav-link nav-toggle">
					<img src="{{ asset('images/sidebar_icons/content_list.png') }}" class="sidebar_icons">
					<span class="title">Content List</span>
					<span class="arrow"></span>
				</a>
			</li> --}}
			
			<!--******************************************** all_tv ********************************************-->
			{{-- <li class="nav-item start parent_li {{ Request::is('admin/screens*') ? 'active' : ''}}">
				<a href="{{ route('admin.screens.index') }}" class="nav-link nav-toggle">
					<img src="{{ asset('images/sidebar_icons/all_tv.png') }}" class="sidebar_icons">
					<span class="title">Screens</span>
				</a>
			</li> --}}

			<!--******************************************** Venues ********************************************-->
			{{-- <li class="nav-item start parent_li {{ Request::is('admin/venues*') ? 'active' : ''}} ">
				<a href="{{ route('admin.venues.index') }}" class="nav-link nav-toggle">
					<img src="{{ asset('images/sidebar_icons/placeholder.png') }}" class="sidebar_icons">
					<span class="title">Venues</span>
					<span class="arrow"></span>
				</a>
			</li> --}}

			<!--******************************************** User Manager ********************************************-->
			{{-- <li class="nav-item start {{ Request::is('admin/users*') ? 'active' : ''}}">
				<a href="{{ route('admin.users.index') }}" class="nav-link nav-toggle">
					<img src="{{ asset('images/sidebar_icons/user_manager.png') }}" class="sidebar_icons">
					<span class="title">User Manager</span>
				</a>
			</li> --}}

			<!--******************************************** settings ********************************************-->
			{{-- <li class="nav-item start {{ Request::is('admin/settings*') ? 'active' : ''}}">
				<a href="{{ route('admin.settings.index') }}" class="nav-link nav-toggle">
					<img src="{{ asset('images/sidebar_icons/settings.png') }}" class="sidebar_icons">
					<span class="title">Settings</span>
					<span class="arrow"></span>
				</a>
			</li>  --}}

			

		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>
<!-- END CRM SIDEBAR -->