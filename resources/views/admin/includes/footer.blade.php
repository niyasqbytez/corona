<!-- FOOTER -->

<!-- BEGIN FOOTER -->
<div class="success_box"></div>
<div class="alert_box_general"></div>
<div class="warning_box_general"></div>
<div class="page-footer">
    <div class="page-footer-inner"> <?php echo date('Y') ?> &copy; Streamline Systems</div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- BEGIN CORE PLUGINS -->

<script src="{{ asset('global/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>


<script src="{{ asset('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"
        type="text/javascript"></script>
<script src="{{ asset('global/scripts/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('layouts/layout/scripts/layout.js') }}" type="text/javascript"></script>


<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
{{--summer note for--}}


<!--DATA TABLES-->
<script src="{{ asset('global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}"
        type="text/javascript"></script>


{{--Jquery UI library--}}
<script src="{{ asset('js/ui-jquery.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/idle-timer.min.js') }}" type="text/javascript"></script>

<!--SELECT2-->
<script src="{{ asset('global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>

<!-- DATE / TIME PICKER -->
<script src="{{ asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('global/plugins/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"
        type="text/javascript"></script>


{{--Compose email--}}
{{--  @include('general.email.compose')  --}}

{{--@if(TASKS_ACCESS_LEVEL != 5)--}}
{{--Add new task model--}}
{{--  @include('general.tasks.task_add')  --}}
{{--@endif--}}
{{-- Quotes tab --}}
{{--  @include('general.quotes.quote_add')  --}}
{{-- Quotes tab --}}

{{--Role delegation popup--}}
{{--  @include('general.role_delegation_popup')  --}}
{{--Role delegation popup--}}

{{--Global functions and variables--}}

<script src="{{asset('global/scripts/ibm.js') }}" type="text/javascript"></script>

<script src="{{ asset('js/intlTelInput.js') }}" type="text/javascript"></script>
{{--  <script src="{{ asset('js/global_script.js') }}" type="text/javascript"></script>  --}}
{{--  <script src="{{ asset('js/tasks.js') }}" type="text/javascript"></script>  --}}
{{--  <script src="{{ asset('js/notification_center.js') }}" type="text/javascript"></script>  --}}
<script src="{{ asset('js/quote.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/croppie.js') }}"></script>
<script src="{{ asset('js/email.js') }}" type="text/javascript"></script>
<script src="{{asset('js/sweetalert.js') }}" type="text/javascript"></script>
<script src="{{asset('js/formstone_core.js')}}"></script>
<script src="{{asset('js/formstone_file_uploader.js')}}"></script>
{{-- <script src="{{asset('js/multi_upload-v1.0.js?v=1.4.7')}}"></script> --}}
{{--  <script src="{{asset('js/file_uploads.js') }}" type="text/javascript"></script>  --}}