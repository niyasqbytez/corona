@extends('admin.layouts.app')

@section('title', 'Business Dashboard')

@section('content')
<!-- TOPBAR -->
<div class="top_bar page-content">
    <!-- LOADER -->
    <div class="loader">
        <div class="top_status_bar load">
            <div class="progress_bar bar" style="left: 0px;">
            </div>
        </div>
    </div>
    <!-- END LOADER -->
    <!-- BREADCRUMBS -->
    <div class="breadcrumbs">
        <ul>
            <li><a href="">Home</a></li>
            {{-- <li><a href="#" class="">Das</a></li> --}}
        </ul>
        <button class="btn btn-large pull-right help theme_btn help_btn">?</button>
    </div>
    <!-- END BREADCRUMBS -->
</div>
<!-- END TOPBAR -->
<!-- PAGE CONTENT -->
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <h2>Dashboard
                            <span class="subtitle"> All the team members registered under employee section listed here, you can filter results based on values for better management.</span>
                        </h2>
                    </div>
                    {{-- <div class="actions success_actions" style="">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#new_filesAdd" class="add_new portal_add_btn">
                            <i class="fa fa-plus"></i>
                        </a>
                        <ul class="nav navbar-nav netsec_menucontent">
                            <li class="actions_dropdown dropdown">
                                <a href="#">Actions</a>
                                <ul class="actions_list dropdown-menu">
                                    <li>
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#new_filesAdd" class="add_new">New Employee</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div> --}}
                </div>
                <?php /*
                <div class="portlet-body flip-scroll">
                    <!--------------------FILTER---------------------->
                    <div class="row form-group">
                        <div class="col-md-2">
                            <select name="column_drop" id="column_drop" class="form-control select2 column_drop select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                <option value=""></option>
                                <option value="display_name">Name</option>
                                <option value="department_uuid">Department</option>
                                <option value="designation_uuid">Designation</option>
                                <option value="status">Employee status</option>
                                <option value="email">Email</option>
                                <option value="mobile">Mobile</option>
                                <option value="whatsapp">Whatsapp</option>
                            </select>
                            
                        </div>
                        <div class="col-md-2 value-drop">
                            <select name="value_drop" id="value_drop" disabled="" class="form-control select2 option_filters select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="col-md-4 pr_0">
                            <button class="theme_btn_round btn filter_all" type="button" name="load" value="Filter" style="">Filter</button>
                            <button class="theme_btn_round btn reset_filter" type="button" name="" value="Clear All" >Clear All</button>
                            <form id="export_excel" method="post" action="" style="display: inline-block;">
                                <input type="hidden" name="_token" value="">                                            
                                <button class="theme_btn_round btn export_excel" style="" type="submit">
                                    Export
                                </button>
                            </form>
                        </div>
                        <div class="column_filters" style="display: block;">
                            <span class="each_filter" data-index="display_name" data-filter="value_drop" data-selected_text="Mr Hazem Galal" data-selected_val="Mr Hazem Galal">
                                Mr Hazem Galal 
                                <i class="fa fa-trash remove_filter"></i>
                            </span>
                        </div>
                        
                        <div class="leads_action_grey actions_selected">
                            <div class="actions_dropdown_grey">
                                <span>Lead options</span>
                                <ul class="actions_list_grey">
                                    <li class="option 1">Option 1</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--------------------FILTER---------------------->

                    <!--------------------Search---------------------->
                    <div class="tb_search">
                        <input type="text" id="search_input_all" onkeyup="FilterkeyWord_all_table()" placeholder="Search.." class="form-control">
                    </div>
                    <table id="table-id" class="table filterable table-striped table-bordered table-hover custom_table">
                        <thead class="flip-content thead">
                            <tr>
                                <th></th>
                                <th class="th_2 ">Name</th>
                                <th class="th_3 ">Employee Id</th>
                                <th class="th_4 ">Department</th>
                                <th class="th_5 ">Designation</th>
                                <th class="th_6 ">Status</th>
                                <th class="th_7 ">Email</th>
                                <th class="th_8 ">Mobile</th>
                                <th class="th_9 ">Whatsapp</th>
                                <th class="th_10 ">Time Spent</th>
                                <th class="th_11 ">Tasks</th>
                                <th class="th_12 ">Opportunities</th>
                                <th class="th_13 ">Leads</th>
                                <th>
                                    <img class="dots_menu_grey" src="images/dots_menu_grey.png">
                                    <div class="dropdown_row table_option_checkboxes" style="">
                                        <span class="dropdown_row_option head">Select Columns</span>
                                        <span class="dropdown_row_option">
                                            <section title="" class="section_squaredThree">
                                                <div class="squaredThree">
                                                    <input type="checkbox" checked="" style="height: auto !important;" name="table_option[]" class="tb_options" value="Name^2" id="Name">
                                                    <label for="Name"></label>
                                                </div>
                                            </section>
                                            <span>Name</span>
                                        </span>
                                        <span class="dropdown_row_option">
                                            <section title="" class="section_squaredThree">
                                                <div class="squaredThree">
                                                    <input type="checkbox" checked="" style="height: auto !important;" name="table_option[]" class="tb_options" value="Employee Id^3" id="Employee Id">
                                                    <label for="Employee Id"></label>
                                                </div>
                                            </section>
                                            <span>Employee Id</span>
                                        </span>
                                        <span class="dropdown_row_option">
                                            <section title="" class="section_squaredThree">
                                                <div class="squaredThree">
                                                    <input type="checkbox" style="height: auto !important;" name="table_option[]" class="tb_options" value="Department^4" id="Department">
                                                    <label for="Department"></label>
                                                </div>
                                            </section>
                                            <span>Department</span>
                                        </span>
                                        <span class="dropdown_row_option">
                                            <section title="" class="section_squaredThree">
                                                <div class="squaredThree">
                                                    <input type="checkbox" style="height: auto !important;" name="table_option[]" class="tb_options" value="Designation^5" id="Designation">
                                                    <label for="Designation"></label>
                                                </div>
                                            </section>
                                            <span>Designation</span>
                                        </span>
                                        
                                        <span class="dropdown_row_option">
                                            <section title="" class="section_squaredThree">
                                                <div class="squaredThree">
                                                    <input type="checkbox" style="height: auto !important;" name="table_option[]" class="tb_options" value="Status^6" id="Status">
                                                    <label for="Status"></label>
                                                </div>
                                            </section>
                                            <span>Status</span>
                                        </span>
                                        
                                        <span class="dropdown_row_option">
                                            <section title="" class="section_squaredThree">
                                                <div class="squaredThree">
                                                    <input type="checkbox" checked="" style="height: auto !important;" name="table_option[]" class="tb_options" value="Email^7" id="Email">
                                                    <label for="Email"></label>
                                                </div>
                                            </section>
                                            <span>Email</span>
                                        </span>
                                        
                                        <span class="dropdown_row_option">
                                            <section title="" class="section_squaredThree">
                                                <div class="squaredThree">
                                                    <input type="checkbox" checked="" style="height: auto !important;" name="table_option[]" class="tb_options" value="Mobile^8" id="Mobile">
                                                    <label for="Mobile"></label>
                                                </div>
                                            </section>
                                            <span>Mobile</span>
                                        </span>
                                        
                                        <span class="dropdown_row_option">
                                            <section title="" class="section_squaredThree">
                                                <div class="squaredThree">
                                                    <input type="checkbox" checked="" style="height: auto !important;" name="table_option[]" class="tb_options" value="Whatsapp^9" id="Whatsapp">
                                                    <label for="Whatsapp"></label>
                                                </div>
                                            </section>
                                            <span>Whatsapp</span>
                                        </span>
                                        
                                        <span class="dropdown_row_option">
                                            <section title="" class="section_squaredThree">
                                                <div class="squaredThree">
                                                    <input type="checkbox" style="height: auto !important;" name="table_option[]" class="tb_options" value="Time Spent^10" id="Time Spent">
                                                    <label for="Time Spent"></label>
                                                </div>
                                            </section>
                                            <span>Time Spent</span>
                                        </span>
                                        
                                        <span class="dropdown_row_option">
                                            <section title="" class="section_squaredThree">
                                                <div class="squaredThree">
                                                    <input type="checkbox" style="height: auto !important;" name="table_option[]" class="tb_options" value="Tasks^11" id="Tasks">
                                                    <label for="Tasks"></label>
                                                </div>
                                            </section>
                                            <span>Tasks</span>
                                        </span>
                                        
                                        <span class="dropdown_row_option">
                                            <section title="" class="section_squaredThree">
                                                <div class="squaredThree">
                                                    <input type="checkbox" style="height: auto !important;" name="table_option[]" class="tb_options" value="Opportunities^12" id="Opportunities">
                                                    <label for="Opportunities"></label>
                                                </div>
                                            </section>
                                            <span>Opportunities</span>
                                        </span>
                                        
                                        <span class="dropdown_row_option">
                                            <section title="" class="section_squaredThree">
                                                <div class="squaredThree">
                                                    <input type="checkbox" style="height: auto !important;" name="table_option[]" class="tb_options" value="Leads^13" id="Leads">
                                                    <label for="Leads"></label>
                                                </div>
                                            </section>
                                            <span>Leads</span>
                                        </span>
                                        <div class="tbl_actions">
                                            <button class="tbl_btn cancel_tbl_option">Cancel</button>
                                            <button class="tbl_btn apply_tbl_option">Apply</button>
                                        </div>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="tbody">
                        <!--=====================-->
                        <!--=====================-->
                            <tr> 
                                <td> 
                                    <div class="img_wrap contact_list">
                                        <div class="dp_name">
                                            <img src="images/avatar2.png" class="dp">
                                        </div>
                                    </div> 
                                </td> 
                                <td> 
                                    <a href="team_view.php" class="editable_name edit restrict_edit">Mr Ahmed Rizvi</a> 
                                </td>
                                <td><span class="employee_id">100098</span></td>
                                <td><span class="department">Sales</span></td>
                                <td><span class="designation_">Account Manager</span></td> 
                                <td ><span class="status">Active</span></td> 
                                <td class="color_theme"><span class="email">rizvi@gbs.com.sa</span></td> 
                                <td class="color_theme"><span class="mobile">+966550887085</span></td>
                                <td class=""><span class="whatsapp">+966550887085</span></td>
                                <td class=""><span class="spent_time">00:00</span></td>
                                <td class=""><span class="total_tasks">0</span></td>
                                <td class=""><span class="ongoing_number_opp">0</span></td>
                                <td class=""><span class="ongoing_number_lead">0</span></td> 
                                <td style="width: 20px;"> 
                                    <img class="dots_menu_grey" src="images/dots_menu_grey.png">
                                    <div class="dropdown_row" style="">
                                        <span class="dropdown_row_option delete_from_list" data-uuid="">Delete</span> 
                                        <span class="dropdown_row_option update_load_form" data-uuid="">Update</span>
                                        <span class="dropdown_row_option view_employee" data-uuid="">View</span>
                                    </div>
                                </td>
                            </tr>
                            <tr> 
                                <td> 
                                    <div class="img_wrap contact_list">
                                        <div class="dp_name">
                                            <img src="images/avatar2.png" class="dp">
                                        </div>
                                    </div> 
                                </td> 
                                <td> 
                                    <a href="team_view.php" class="editable_name edit restrict_edit">Mr Hazem Galal</a> 
                                </td> 
                                <td><span class="employee_id">100145</span></td>
                                <td><span class="department">Sales</span></td>
                                <td><span class="designation_">Account Manager</span></td> 
                                <td><span class="status">Active</span></td> 
                                <td class="color_theme"><span class="email">hazem@gbs.com.sa</span></td> 
                                <td class="color_theme"><span class="mobile">+966596993369</span></td>
                                <td class=""><span class="whatsapp">+966596993369</span></td>
                                <td class=""><span class="spent_time">00:00</span></td>
                                <td class=""><span class="total_tasks">0</span></td>
                                <td class=""><span class="ongoing_number_opp">54</span></td>
                                <td class=""><span class="ongoing_number_lead">0</span></td> 
                                <td style="width: 20px;"> 
                                    <img class="dots_menu_grey" src="images/dots_menu_grey.png">
                                    <div class="dropdown_row">
                                        <span class="dropdown_row_option delete_from_list" data-uuid="">Delete</span> 
                                        <span class="dropdown_row_option update_load_form" data-uuid="">Update</span>
                                        <span class="dropdown_row_option view_employee" data-uuid="">View</span>
                                    </div>
                                </td>
                            </tr>


                            
                        </tbody>
                    </table>

                    <div class="bottom_custom">
                        <div class="rows">
                            <select class="form-control" name="state" id="maxRows">
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                                <option value="50" selected="">50</option>
                                <option value="100">100</option>
                                <option value="1000">1000</option>
                            </select>
                            <span>Records</span>
                        </div>
                        <div class="pagination-container">
                            <button class="load_more_listing" last_id="2" disabled="disabled">
                                <span class="load_more_list" style="display: inline;">Load More</span> 
                                <span class="list_loading" style="display: none;">Loading 
                                    <i class="fa fa-spinner fa-spin"></i>
                                </span>
                            </button>
                        </div>
                        <div class="rows_count">Showing 10 of 37 entries</div>
                        <span class="now_showing_count hide">37</span>
                        <span class="total_records hide">37</span>
                    </div>
                </div>
                */?>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
</div>
<!-- END PAGE CONTENT -->
@stop
@section('styles')
   
@stop
@section('admin_js')

@stop