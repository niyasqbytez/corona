$(document).ready(function (e) {
    //MINHEIGHT SET
    $(function () {
        var content_height = $('.page-content').height();
        $('.notifications_wrapper').height(content_height);
    });
    //MINHEIGHT SET
    //SELECT2 Simple
    $(".select2").select2({
        placeholder: "--Select--",
        allowClear: true
    });
    //SELECT2 Simple
    // SELECT 2 WITH TAG SUPPORT
    $(".select2-tag").select2({
        placeholder: "--Select--",
        tags: true
    });


    //DATABLES SIMPLE

    $('.datatables_simple').DataTable({
        "dom": '<"top"f>rt<"bottom"lpi><"clear">',
        language: {search: ""},
        "bSort": false
    });

    $('.dataTables_filter input').attr('placeholder', 'Search');


    //RESTRICT DATE FROM YEAR TO DATE//

    var date = new Date();
    var year = date.getFullYear();

    $(document).ready(function () {
        // Year to Current Date
        $('.ytd').datepicker({
            format: "dd-mm-yyyy",
            endDate: '+0d',
            autoclose: true,
            startDate: "01-01-" + year
        });

        // Previous date lock
        $('.pdl').datepicker({
            format: 'dd-mm-yyyy',
            pickerPosition: "bottom-left",
            autoclose: true,
            startDate: date
        });

        // Future date lock
        $('.fdl').datepicker({
            format: 'dd-mm-yyyy',
            pickerPosition: "bottom-left",
            autoclose: true,
            endDate: date
        });

        // Top Left orientation
        $('.tlo').datepicker({
            format: 'dd-mm-yyyy',
            pickerPosition: "top-left",
            autoclose: true
        });

    });


    /*
     |--------------------------------------------------------------------------
     | Date Picker
     |--------------------------------------------------------------------------
     */

    //Previous date blocked
    $('.calender_popup').datepicker({
        /*format: 'dd-mm-yyyy',
        pickerPosition: "bottom-left",
        autoclose: true,*/
        format: 'dd-mm-yyyy',
        pickerPosition: "bottom-left",
        autoclose: true,
        startDate: date
    }).on('hide.bs.modal', function (event) {
        // prevent datepicker from firing bootstrap modal "show.bs.modal"
        event.stopPropagation();
    });

    $('body').on('click', '.custom_calender', function () {
        $(this).parent().parent().find('.calender_popup').trigger('focus');
    });


    //Date open either previous or upcoming
    $('.calender_popup_date_open').datepicker({
        /*format: 'dd-mm-yyyy',
         pickerPosition: "bottom-left",
         autoclose: true,*/
        format: 'dd-mm-yyyy',
        pickerPosition: "bottom-left",
        autoclose: true,
        //startDate: date
    }).on('hide.bs.modal', function (event) {
        // prevent datepicker from firing bootstrap modal "show.bs.modal"
        event.stopPropagation();
    });

    $('body').on('click', '.custom_calender_date_open', function () {
        $(this).parent().parent().find('.calender_popup_date_open').trigger('focus');
    });


    /*
     |--------------------------------------------------------------------------
     | TIME Picker
     |--------------------------------------------------------------------------
     */

    $('.time_popup').timepicker({
        autoclose: true,
        showMeridian: false,
        maxHours: 24,
        // pickerPosition: "bottom-left",
        minuteStep: 5,
        defaultTime: 'current'
    });

    $('.custom_timePick').click(function (event) {
        event.preventDefault();
        $(this).parent().find('input').click();
    });

    //INPUT ONLY NUMBER IN FIELD RESTRICTION//

    // $('body').on('keydown paste', '.quantity, .rate, .adjustment_tax_2, .total_discount, .number_only, .flat, .percentage, .overall_flat, .overall_percentage, .qty, #opening_balance_amount, #amount_transfer, .balance, .advance_drawings, #cradit_limit, .emp_wages, .emp_overtime, .just_number', function (e) {
    //     var key = e.keyCode ? e.keyCode : e.which;
    //     if (!([8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
    //             (key == 65 && (e.ctrlKey || e.metaKey)) ||
    //             (key >= 35 && key <= 40) ||
    //             (key != 86 && (e.ctrlKey || e.metaKey)) ||
    //             (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
    //             (key >= 96 && key <= 105) || (e.keyCode == 45)
    //
    //         )) {
    //         e.preventDefault();
    //     } else {
    //         // console.log('ok');
    //     }
    // });


    $('body').on('keydown', '.quantity, .rate, .adjustment_tax_2, .total_discount, .number_only, .flat, .percentage, .overall_flat, .overall_percentage, .qty, #opening_balance_amount, #amount_transfer, .balance, .advance_drawings, #cradit_limit, .emp_wages, .emp_overtime, .just_number', function (e) {
        var key = e.keyCode ? e.keyCode : e.which;
//         (key == 86 && (e.ctrlKey || e.metaKey)) ||


        if (!([8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
                (key == 65 && (e.ctrlKey || e.metaKey)) ||
                (key >= 35 && key <= 40) ||
                (key == 86) ||
                (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
                (key >= 96 && key <= 105) || (e.keyCode == 45)

            )) {
            e.preventDefault();
        } else {

            if (key == 86) {

            }

        }
    });


    if ($('.report_view_wrap').length > 0) {

        if (($('#startDate').length > 0) && ($('#endDate').length > 0)) {

            $('#startDate, .btn.default.calender, #endDate').attr('disabled', 'disabled');
        }
    }
});

$(window).load(function () {
    $('#busyloading').hide();
});


// FOR ACTION DROP DOWN TOP //
$('body').on('click', '.actions_dropdown', function (event) {
    //     $(".actions_dropdown").unbind().click(function(event) {
    if ($(this).hasClass("active_actions")) {
        var el = $(this),
            curHeight = el.height(),
            autoHeight = el.css('height', '34px').height();
        el.height(curHeight).animate({height: autoHeight}, 500);
        $(this).removeClass('active_actions');
        event.stopPropagation();
    } else {
        var el = $(this),
            curHeight = el.height(),
            autoHeight = el.css('height', 'auto').height();
        el.height(curHeight).animate({height: autoHeight}, 500);
        $(this).addClass('active_actions');
        event.stopPropagation();
    }

});

$(window).click(function () {
    // if ($('.actions_dropdown').hasClass("active_actions")) {

    if (($('.actions_dropdown').height()) > 34) {
        var el = $('.actions_dropdown'),
            curHeight = el.height(),
            autoHeight = el.css('height', '34px').height();
        el.height(curHeight).animate({height: autoHeight}, 500);
        $(el).removeClass('active_actions');
    }

    // }

});


// FOR ACTION DROP DOWN TOP CONTACT //
$('body').on('click', '.actions_dropdown_grey', function (event) {
    //     $(".actions_dropdown").unbind().click(function(event) {
    if ($(this).hasClass("active_actions_grey")) {
        var el = $(this),
            curHeight = el.height(),
            autoHeight = el.css('height', '34px').height();
        el.height(curHeight).animate({height: autoHeight}, 500);
        $(this).removeClass('active_actions_grey');
        event.stopPropagation();
    } else {
        var el = $(this),
            curHeight = el.height(),
            autoHeight = el.css('height', 'auto').height();
        el.height(curHeight).animate({height: autoHeight}, 500);
        $(this).addClass('active_actions_grey');
        event.stopPropagation();
    }

});

$(window).click(function () {
    // if ($('.actions_dropdown').hasClass("active_actions")) {

    if (($('.actions_dropdown_grey').height()) > 34) {
        var el = $('.actions_dropdown_grey'),
            curHeight = el.height(),
            autoHeight = el.css('height', '34px').height();
        el.height(curHeight).animate({height: autoHeight}, 500);
        $(el).removeClass('active_actions_grey');
    }

    // }

});

// User options in top menu
$('body').on('click', '.user_options', function () {
    $('.user_drop_menu').slideToggle('slow');
});

$(window).click(function () {
    if ($('.user_drop_menu:visible').length != 0) {
        $('.user_drop_menu').slideToggle('slow');
    }
});

$('body').on('click', '.user_options', function (event) {
    event.stopPropagation();
});


// Focus input on click
$('body').on('focus', 'input, textarea', function () {
    $(this).select();
});

//TOP MENU BAR//

$('body').on('click', '.menu_bar_toggle', function () {
    $('.nav_bar_menu').slideToggle("slow");

});

$('body').on('click', '.menu_bar_toggle, .inner_wrap', function (event) {
    event.stopPropagation();
});

$(window).click(function () {

    $('.nav_bar_menu').slideUp("slow");

});

//TOP MENU BAR//


//SMALL GREY DOTS DROP DOWN//
$('body').on('click', '.dots_menu_grey', function () {

    if ($(this).hasClass('active')) {
        $(this).parent().find('.dropdown_row').slideUp('slow');
        $('.dots_menu_grey').removeClass('active');
    } else {
        $('.dots_menu_grey').removeClass('active');
        $('.dropdown_row').slideUp('slow');
        $(this).parent().find('.dropdown_row').slideDown('slow');
        $(this).addClass('active');
    }
});


$('body').on('click', '.dots_menu_grey, .dropdown_row', function (event) {
    event.stopPropagation();
});

$(window).click(function () {
    $('.dots_menu_grey').removeClass('active');
    $('.dropdown_row').removeClass('open').slideUp('slow');
});

// Slideup table options
$('.cancel_tbl_option').click(function () {
    $('.dots_menu_grey').removeClass('active');
    $('.dropdown_row').removeClass('open').slideUp('slow');
})
//SMALL GREY DOTS DROP DOWN//


// Multi action options


$('body').on('click', '.drop', function () {
    $('.popup_select').slideToggle();
    $('.popup_select_bottom').slideToggle();
});

$(window).click(function () {
    $('.popup_select').slideUp();
    $('.popup_select_bottom').slideUp();
});
$('body').on('click', '.drop', function (event) {
    event.stopPropagation();
});

$('.popup_select li, .popup_select_bottom li').click(function () {
    var btn_select = $(this).attr('value');

    $('.actions_multi_options ').hide();
    $('.actions_multi_options').attr('id', '');

    $('.' + btn_select).attr('id', 'active_option').show().removeClass('hide');

    $('.popup_select, .popup_select_bottom ').slideUp();

    // Task reasons box open // for normal drop down it will b in condition
    task_actions(btn_select);


});


function task_actions(btn_select) {
    if (btn_select == 'save_escalate') {
        $('#new_task .actions_div').show();
        $('#new_task .body_wrap').hide();
    }
}

$('body').on('click', '.go_back_task', function () {
    $('#new_task .actions_div').hide();
    $('#new_task .body_wrap').show();
});


//FILTERS DATE RANGE SETTING FOR SPECIALLY REPORTS//

// $('body').on('change', '.duration_by', function () {
//
//     var filter_by = $(this).val();
//
//     if (filter_by != 'custom') {
//         $('#busyloading').show();
//
//         $.post("<?php echo SITE_ROOT ?>All_Reports/report_filter", {
//             filter_by: filter_by
//         }).done(function (res) {
//
//             var result = $.parseJSON(res);
//
//             $('#startDate').val(result['startdate']).attr('disabled', 'disabled');
//             $('#endDate').val(result['enddate']).attr('disabled', 'disabled');
//
//             $('#busyloading').hide();
//         });
//     } else {
//         $('#startDate, .btn.default.calender, #endDate').removeAttr('disabled');
//         // $('').removeAttr('disabled');
//     }
// });

$("form").on('submit', function () {

    $('#startDate, .btn.default.calender, #endDate').removeAttr('disabled');

});


/*
 |--------------------------------------------------------------------------
 | For phone numbers
 |--------------------------------------------------------------------------
 */
$("input.phone").intlTelInput({
    // allowDropdown: false,
    // autoHideDialCode: false,
    // autoPlaceholder: "off",
    // dropdownContainer: "body",
    // excludeCountries: ["us"],
    // formatOnDisplay: false,
    // geoIpLookup: function(callback) {
    //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
    //     var countryCode = (resp && resp.country) ? resp.country : "";
    //     callback(countryCode);
    //   });
    // },
    // hiddenInput: "full_number",
    initialCountry: (BUSINESS_COUNTRY_PREFIX == 'ksa') ? 'sa' : BUSINESS_COUNTRY_PREFIX,
    // localizedCountries: { 'ae': 'United Arab Emirates' },
    // nationalMode: false,
    // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
    // placeholderNumberType: "MOBILE",
    preferredCountries: ['ae', 'sa'],
    // separateDialCode: true,
    utilsScript: asset_url + "/js/utils.js"
});


/*
 |--------------------------------------------------------------------------
 | For Amount formatting
 |--------------------------------------------------------------------------
 */

$('.amount').focusout(function () {
    var num = $(this).val();
    $('.lead_value').val(num);
    updateValAmount(num)
});

function updateValAmount(num) {
    //$('.amount').val(parseFloat(num).toFixed(AMOUNT_DECIMAL).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
    var amount_val = $('.amount').val();
    var result = amount_val.split('.');
    var amount_decimal = '';

    if (typeof result[1] !== "undefined") {
        result[1] = result[1].replace(/\.?0+$/, '');
        amount_decimal = '.' + result[1];
    }
    $('.amount').val(result[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") + amount_decimal);
}

$('.amount').focusin(function () {
    var num = $('.lead_value').val();
    $(this).val(num);
    //$(this).val(parseFloat(num).toFixed(AMOUNT_DECIMAL).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
});


function amount_conversion_listing(val_amount) {
    return parseFloat(val_amount).toFixed(AMOUNT_DECIMAL).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

// ETA time validation

// $('body').on('focus', '.estimation_eta', function (e) {
//     // $('.task_eta').numeric({allow: "dhm "});
// });
//
// function get_time_val() {
//     $('.task_eta').val();
// }
//
// var time_val = '';
// $('body').on('input', '.estimation_eta', function (e) {
//     time_val = $(this).val();
//     // input in app.blade
//     $('.time_val').val(time_val);
// });

//.estimation_eta


$('body').on('keypress', '.estimation_eta', function (e) {

    var input_val = $(this).val();
    // var entered_val = $('.time_val').val();
    var last_char = input_val.slice(-1);

    // restrict if the format is wrong
    var keynum = e.which;
    var keyletter = String.fromCharCode(keynum);

    // Allow backspace
    if (e.which == 32) {
        if (last_char == 'd' || last_char == 'h' || last_char == 'm') {
            return true;
        } else {
            return false;
        }
    }

    if (e.which == '37' || e.which == 39) {
        return true;
    }

    // Backspace allow
    if (keynum == 8 || keynum == 46) {
        return true;
        // var stop = '1';
    }

// If minutes already in string then stop user
    if (input_val.indexOf('m') != -1) {
        return false;
    }

// if entered number then allow
    if (e.which >= 48 && e.which <= 57) {
        return true;
    }


// enter space when d or h pressed
//     if (last_char == 'd' || last_char == 'h') {
//
//         console.log(last_char);
//         if (last_char == 'd') {
//
//             $(this).val(entered_val + ' ');
//         }
//         if (last_char == 'h') {
//             $(this).val(entered_val + ' ');
//         }
//
//     }

// only d,m,h letters allow
    if (keyletter == 'd' || keyletter == 'h' || keyletter == 'm') {

// if string contains 'h' already then stop user to enter 'd'
        if (input_val.indexOf('h') != -1) {

            if (keyletter == 'd') {
                return false;
            }

        }

    } else {
        return false;
    }
});

// function to check the right format for eta. on submit

function validate_eta(string) {

    var split = string.split(' ');
    var flag = 0;

    $.each(split, function (i, v) {
        if (v != '') {

            if (v.indexOf('d') != -1 || v.indexOf('h') != -1 || v.indexOf('m') != -1) {
                // do nothing
            } else {
                flag = 1;
            }
        }
    });


    if (flag == 1) {
        return false;
    } else {
        return true;
    }

}


// check spaces in ETA

function validate_eta_space(string) {
    var flag = 0;
    if (string.indexOf('d') != -1 && string.indexOf('h') != -1 && string.indexOf('m') != -1) {
        var split = string.split(' ');
        if (split.length >= 3) {

        } else {
            flag = 1
        }
    } else if (string.indexOf('d') != -1 && string.indexOf('h') != -1) {
        var split_ = string.split(' ');
        if (split_.length >= 2) {

        } else {
            flag = 1
        }
    }
    else if (string.indexOf('d') != -1 && string.indexOf('m') != -1) {
        var split_ = string.split(' ');
        if (split_.length >= 2) {

        } else {
            flag = 1
        }
    }

    else if (string.indexOf('h') != -1 && string.indexOf('m') != -1) {
        var split_ = string.split(' ');
        if (split_.length >= 2) {

        } else {
            flag = 1
        }
    }
    else if (string.indexOf('m') != -1) {
        var split_ = string.split(' ');
        if (split_.length >= 1) {

        } else {
            flag = 1
        }
    }
    else if (string.indexOf('h') != -1) {
        var split_ = string.split(' ');
        if (split_.length >= 1) {

        } else {
            flag = 1
        }
    }


    if (flag == 1) {
        return false;
    } else {
        return true;
    }
}

// For new popup open from quick button add button
$(function () {
    var c_url = window.location.pathname;
    // c_url.slice(0,-2);
    var hostname = window.location.hostname;

    var pathArray = window.location.pathname.split('/');
    var path = pathArray[4] + '/' + pathArray[5] + '/' + pathArray[6] + '/' + pathArray[7];


    if (typeof POPUP != 'undefined') {

        if (POPUP == '1') {
            $('.add_new').trigger('click');
            history.pushState(null, null, c_url.slice(0, -2));
        } else if (typeof pathArray[4] != 'undefined') {
            $('.column_filters').show();
            $('.reset_filter').show();
            history.pushState(null, null, c_url.slice(0, -path.length));
        }
    }
});


// All Table search script
function FilterkeyWord_all_table() {

// Count td if you want to search on all table instead of specific column

    var count = $('.table').children('tbody').children('tr:first-child').children('td').length;

    // Declare variables
    var input, filter, table, tr, td, i;
    input = document.getElementById("search_input_all");
    var input_value = document.getElementById("search_input_all").value;
    filter = input.value.toLowerCase();
    if (input_value != '') {
        table = document.getElementById("table-id");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 1; i < tr.length; i++) {

            var flag = 0;

            for (j = 0; j < count; j++) {
                td = tr[i].getElementsByTagName("td")[j];
                if (td) {

                    var td_text = td.innerHTML;
                    if (td.innerHTML.toLowerCase().indexOf(filter) > -1) {
                        //var td_text = td.innerHTML;
                        //td.innerHTML = 'shaban';
                        flag = 1;
                    } else {
                        //DO NOTHING
                    }
                }
            }
            if (flag == 1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    } else {
        //RESET TABLE
        $('#maxRows').trigger('change');
    }
}