//ADD NEW DROPDOWN START
$(document).ready(function(){
    $(".actions_dropdown").click(function(){
        $(this).find(".actions_list").slideToggle();

    });
    
});
$(document).on("click", function(event){
    var $trigger = $(".actions_dropdown");
    if($trigger !== event.target && !$trigger.has(event.target).length){
        $(".actions_list").slideUp();
    }            
});

$(document).ready(function(){
    //SELECT2 DROPDOWN START
    $(".select2").select2({
        placeholder: "--Select--",
        allowClear: true
    });

    //SMALL GREY DOTS DROP DOWN//
    $('body').on('click', '.dots_menu_grey', function () {
        if ($(this).hasClass('active')) {
            $(this).parent().find('.dropdown_row').slideUp('slow');
            $('.dots_menu_grey').removeClass('active');
        } else {
            $('.dots_menu_grey').removeClass('active');
            $('.dropdown_row').slideUp('slow');
            $(this).parent().find('.dropdown_row').slideDown('slow');
            $(this).addClass('active');
        }
    });
    $('body').on('click', '.dots_menu_grey, .dropdown_row', function (event) {
        event.stopPropagation();
    });
    $(window).click(function () {
        $('.dots_menu_grey').removeClass('active');
        $('.dropdown_row').removeClass('open').slideUp('slow');
    });
    // Slideup table options
    $('.cancel_tbl_option').click(function () {
        $('.dots_menu_grey').removeClass('active');
        $('.dropdown_row').removeClass('open').slideUp('slow');
    })
    //SMALL GREY DOTS DROP DOWN//
});
$('body').on('keydown', '.quantity, .rate , .number_only, .flat, .percentage, .overall_flat, .overall_percentage, .qty, #opening_balance_amount, #amount_transfer, .balance, .advance_drawings, #cradit_limit, .percentage_value, .emp_overtime, .just_number', function (e) {
    var key = e.keyCode ? e.keyCode : e.which;
//         (key == 86 && (e.ctrlKey || e.metaKey)) ||


    if (!([8, 9, 13, 27, 46, 110, 190].indexOf(key) !== -1 ||
            (key == 65 && (e.ctrlKey || e.metaKey)) ||
            (key >= 35 && key <= 40) ||
            (key == 86) ||
            (key >= 48 && key <= 57 && !(e.shiftKey || e.altKey)) ||
            (key >= 96 && key <= 105) || (e.keyCode == 45)

        )) {
        e.preventDefault();
    } else {

        if (key == 86) {
            e.preventDefault();
        }

    }
});