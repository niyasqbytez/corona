
// validate null values in form

function validationForm(form) {
    var flag = 0;
    $(form + ' *').filter(':input, select, textarea').each(function () {
        if ($(this).hasClass('validate')) {
            if (($(this).val() == '') || ($(this).val() === null)) {
                $(this).addClass('error_custom');
                $(this).parent().find('.select2-container .select2-selection').addClass('error_custom');
                flag = 1;
            } else {
                $(this).removeClass('error_custom');
                $(this).parent().find('.select2-container .select2-selection').removeClass('error_custom');
            }
        }
    });
    return flag;
}
