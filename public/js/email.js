//For Settings
$('.email_settings').click(function (e) {
    e.preventDefault();
    /*alert( /^mail/.test($('.mail_server_host').val()) );
    return false;*/
    var flag = '0';
    $('#email_settings *').filter(':input').each(function () {
        if ($('.password_set').val() == 1) {
            $('.incoming_password').removeClass('validate');
        } else {
            $('.incoming_password').addClass('validate');
        }
        if ($(this).hasClass('validate')) {
            if ($(this).val() == '') {
                $(this).addClass('error_custom');
                flag = '1';
            } else {
                $(this).removeClass('error_custom');
            }
        }

        /*FOR EMAIL VALIDATION*/
        if (($(this).is('.user_email')) && ($(this).val() != '')) {
            var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
            if (testEmail.test($(this).val())) {
                $(this).removeClass('error_custom');
            } else {
                $(this).addClass('error_custom');
                flag = '1';
            }
        }
        /*FOR EMAIL VALIDATION*/


    });
    if (flag == '1') {
        alert_msg('There are errors on page');
        return false;
    } else {
        $('#busyloading').show();
        var formData = new FormData(document.getElementById("email_settings"));
        formData.append('submit', 1);
        $.ajax({
            url: APP_URL + '/email_settings',
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (res) {
                var result = $.parseJSON(res);
                if (result.success > 0) {
                    success_msg('Email settings are successfully updated.');
                } else if (result.success == 0) {
                    warning_msg('Nothing to update.');
                } else {
                    alert_msg('Something went wrong. Please try again later or contact to administrator.');
                }
                $('#busyloading').hide();
                return false;
            }
        });
    }
});

//Settings view to display block once click on Settings
$('.email_settings_section_link').click(function () {
    $('.email_section').fadeOut('slow', function () {
        $('.email_settings_section').fadeIn('slow');
    });
    $('.close_detail').trigger('click');
});

//Get email by folders
$('body').on('click', '.get_emails', function (e) {
    e.preventDefault();

    $('.msg_action').hide();
    $('.all_msgs').prop('checked', false);

    var folder_name = $(this).attr('data-type');
    $('.move_emails').show();
    $(".folders_move").find("[folder-data='" + folder_name + "']").hide();


    $('.title_folder').text(folder_name.replace("[Gmail]/", ""));
    $('.inner_msg_body_wrap').html('');
    $('.close_detail').trigger('click');

    if ($('.email_settings_section').is(':visible')) {
        $('.email_settings_section').fadeOut('slow', function () {
            $('.email_section').fadeIn('slow');
        });
    }

    $('.get_emails').parent().removeClass('active');
    $(this).parent().addClass('active');


    $('#busyloading').show();
    var formData = new FormData();
    formData.append('folder_name', folder_name);
    formData.append('_token', CSRF_TOKEN);
    $.ajax({
        url: APP_URL + '/get_email_by_folder',
        type: 'POST',
        data: formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success: function (res) {
            var result = $.parseJSON(res);
            console.log(result);
            if (result.error == '') {
                if ((result.emails).length > 0) {
                    var limit = $('.email_list_limit').val();
                    var folder = $('.email_folder').val(folder_name);
                    $('.emails_loaded').val(limit);
                    $('.total_emails').val(result.total);

                    $.each(result.emails, function (i, v) {
                        var clone = $('.message_clone').clone();
                        clone.removeClass('message_clone').removeClass('hide_important');
                        clone.find('.each_msg_check').prop('checked', false);

                        if (EMAIL_INCOMING_PORT == 995) {
                            clone.find('.each_msg_check').attr('id', 'each_msg_' + v.date);
                            clone.find('.each_msg_check').val(v.date);
                            clone.find('.check_label').attr('for', 'each_msg_' + v.date);

                            clone.find('.open_message').attr('data-msg_uuid', '');
                            clone.find('.open_message').attr('data-folder', v.mailbox);
                            clone.find('.open_message').removeClass('open_message').addClass('open_this');
                        } else {
                            clone.find('.each_msg_check').attr('id', 'each_msg_' + v.uid);
                            clone.find('.each_msg_check').val(v.uid);
                            clone.find('.check_label').attr('for', 'each_msg_' + v.uid);

                            clone.find('.open_message').attr('data-msg_uuid', v.uid);
                            clone.find('.open_message').attr('data-folder', v.mailbox);
                        }

                        if ($.inArray("seen", v['flags']) !== -1) {
                            clone.find('.envelope > i').removeClass('fa-envelope').addClass('fa-envelope-open-o');
                        } else if ($.inArray("unseen", v['flags'])) {
                            clone.find('.envelope > i').addClass('fa-envelope').removeClass('fa-envelope-open-o');
                            clone.addClass('un_read');
                        }


                        if (v.attachment == true) {
                            clone.find('.message_subject').html('<i class="fa fa-paperclip"></i>');
                        } else {
                            clone.find('.message_subject').html('');
                        }
                        clone.find('.message_subject').append(v.subject);
                        clone.find('.email_date').html(v.date);

                        clone.find('.message_from').html();

                        var str1 = folder_name;
                        var str2 = "sent";
                        var str3 = "Sent";
                        if (str1.indexOf(str2) != -1 || str1.indexOf(str3) != -1) {
                            if (v['to'].length > 0) {
                                $.each(v['to'], function (i, j) {
                                    if (i == eval((v.to).length) - eval(1)) {
                                        clone.find('.message_from').append((j.name == null) ? j.email : j.name + ' <' + j.email + '>');
                                    } else {
                                        clone.find('.message_from').append((j.name == null) ? j.email : j.name + ' <' + j.email + '>, ');
                                    }
                                });
                            } else {
                                clone.find('.message_from').html(v['to']['email']);
                            }
                        } else {
                            if (v['from']['name'] !== null) {
                                clone.find('.message_from').html(v['from']['name'] + ' <' + v['from']['email'] + '>');
                            } else {
                                clone.find('.message_from').html(v['from']['email']);
                            }
                        }


                        $('.inner_msg_body_wrap').append(clone);

                    });
                } else {
                    var clone = $('.message_clone').clone();
                    clone.removeClass('message_clone').removeClass('hide_important');

                    clone.find('.col-md-12').html('<div class="no_email">No email found in ' + folder_name + '.</div>');

                    $('.inner_msg_body_wrap').append(clone);
                }
            } else if (result.error != '') {
                warning_msg(result.error);
            } else {
                alert_msg('Something went wrong. Please try again later or contact to administrator.');
            }
            $('#busyloading').hide();
            return false;
        }
    });
});

//Get email by email uid and folder name
$('body').on('click', '.open_message', function (e) {
    e.preventDefault();

    var parent_selecter = $(this).parent().parent().parent();
    $('.each_msg').removeClass('opened');
    var message_uuid = $(this).attr('data-msg_uuid');
    var folder = $(this).attr('data-folder');
    $('#busyloading').show();
    var formData = new FormData();
    formData.append('message_uuid', message_uuid);
    formData.append('folder', folder);
    formData.append('_token', CSRF_TOKEN);
    $.ajax({
        url: APP_URL + '/get_email_by_uuid',
        type: 'POST',
        data: formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success: function (res) {
            var result = $.parseJSON(res);
            // console.log(result);
            if (result.error == '') {
                parent_selecter.addClass('opened');
                if ($('.opened').hasClass('un_read')) {
                    if (folder == 'INBOX' || folder == 'inbox') {
                        var number = eval($('.folder_count').html()) - eval(1);
                        if (number < 1) {
                            number = '';
                        }
                        $('.folder_count.inbox').html(number);
                    }
                    $('.opened').removeClass('un_read');
                    $('.opened').find('span.envelope > i').removeClass('un_read').addClass('fa-envelope-open-o').removeClass('fa-envelope');
                }

                /*clone.find('.envelope > i');
                clone.addClass('un_read');*/
                $('.linked_btn').attr('msg_uid', message_uuid) // Link to object btn

                $('.msg_body_detail .subject').html(result.email.topic);
                $('.msg_body_detail .message_from').html((result.email.from.name == null) ? result.email.from.email : result.email.from.name + ' <' + result.email.from.email + '>');
                $('.msg_body_detail .message_from_email').html(result.email.from.email);
                $('.msg_body_detail span.folder_name').html((result.email.mailbox).replace("[Gmail]/", ""));
                $('.download_attac').attr('data-uuid', result.email.uid);
                $('.delete_message').attr('data-email-UID', result.email.uid);
                $('.delete_message').attr('data-email-folder', result.email.mailbox);
                $('.download_attac').attr('data-from', result.email.from.email);

                if (result.email.from.name !== null) {
                    var str = result.email.from.name;
                    var result1 = str.split(' ');
                    var first = result1[0].charAt(0);
                    var last = '';
                    if (result1.length > 1) {
                        last = result1[1].charAt(0);
                    }
                    $('.msg_body_detail .img_txt_wrap').html(first + last);
                } else {
                    var str = result.email.from.email;
                    var result1 = str.split('@');
                    var first = result1[0].charAt(0);
                    var last = result1[0].charAt((result1[0].length - 1));
                    $('.msg_body_detail .img_txt_wrap').html(first + last);
                }

                $('.msg_body_detail .date_time').html('');
                if (typeof result.email.attachment !== 'undefined' && (result.email.attachment).length > 0) {
                    $('.msg_body_detail .date_time').html('<i class="fa fa-paperclip"></i>');
                }
                $('.msg_body_detail .date_time').append(' ' + result.email.date);

                if ((result.email.to).length > 0) {
                    $('.msg_body_detail .msg_to').html('To : ');
                    $.each(result.email.to, function (i, v) {
                        if (i == eval((result.email.to).length) - eval(1)) {
                            $('.msg_body_detail .msg_to').append((v.name == null) ? v.email : v.name + ' <' + v.email + '>');
                        } else {
                            $('.msg_body_detail .msg_to').append((v.name == null) ? v.email : v.name + ' <' + v.email + '>, ');
                        }
                    });
                }

                //CC
                if ((result.email.cc).length > 0) {
                    $('.msg_body_detail .msg_cc').show();
                    $('.msg_body_detail .msg_cc').html('CC : ');
                    $.each(result.email.cc, function (i, v) {
                        if (i == eval((result.email.cc).length) - eval(1)) {
                            $('.msg_body_detail .msg_cc').append((v.name == null) ? v.email : v.name + ' <' + v.email + '>');
                        } else {
                            $('.msg_body_detail .msg_cc').append((v.name == null) ? v.email : v.name + ' <' + v.email + '>');
                            $('.msg_body_detail .msg_cc').append(', ');
                        }
                    });
                } else {
                    $('.msg_body_detail .msg_cc').hide();
                }

                //BCC
                if ((result.email.bcc).length > 0) {
                    $('.msg_body_detail .msg_bcc').show();
                    $('.msg_body_detail .msg_bcc').html('BCC : ');
                    $.each(result.email.bcc, function (i, v) {
                        if (i == eval((result.email.bcc).length) - eval(1)) {
                            $('.msg_body_detail .msg_bcc').append((v.name == null) ? v.email : v.name + ' <' + v.email + '>');
                        } else {
                            $('.msg_body_detail .msg_bcc').append((v.name == null) ? v.email : v.name + ' <' + v.email + '>');
                            $('.msg_body_detail .msg_bcc').append(', ');
                        }
                    });
                } else {
                    $('.msg_body_detail .msg_bcc').hide();
                }

                if (typeof result.email.body['text/html'] !== 'undefined' && result.email.body['text/html'] != '') {
                    $('.msg_body_detail .each_msg_detail').html(result.email.body['text/html']);
                } else {
                    $('.msg_body_detail .each_msg_detail').html(result.email.body['text/plain']);
                }

                //Attachments
                //console.log((result.email.attachment).length);
                if (result.attachments > 0) {
                    $('.msg_body_detail .msg_attachment_wrap').show();
                    $('.msg_body_detail .attac_count').html(result.attachments + ' Attachment');
                    $('.attachments').html('');
                    $.each(result.email.attachment, function (i, v) {
                        var clone = $('.attachment_clone').clone();
                        clone.removeClass('attachment_clone').removeClass('hide_important');

                        clone.find('.attachment_img').attr('src', v.src);
                        clone.find('.img_name').html(v.name);
                        clone.find('.img_name').parent().attr('href', v.src);
                        clone.find('.img_size').html(v.size);

                        $('.attachments').append(clone);
                    });
                } else {
                    $('.msg_body_detail .msg_attachment_wrap').hide();
                    $('.msg_body_detail .each_attachment').remove();
                    /*var clone = $('.attachment_clone').clone();
                    clone.find('.msg_attachment_wrap').remove();*/
                }


                $('.email_section.menu_body').removeClass('col-md-10').addClass('col-md-4');
                $('.msg_body_detail').removeClass('col-md-10').addClass('col-md-6').fadeIn(500);
                // $('.msg_body_detail').fadeIn(500);

                if (result.linked_objects_name != '') {
                    $('.linked_objects').show();
                    $('.linked_objects_name').text(result.linked_objects_name);
                } else {
                    $('.linked_objects').hide();
                }


                $('.linked_with_tags').remove();

                //success_msg('Email are loaded.');
            } else if (result.error != '') {
                warning_msg(result.error);
            } else {
                alert_msg('Something went wrong. Please try again later or contact to administrator.');
            }
            $('#busyloading').hide();
            return false;
        }
    });
});

//Delete email by email uid
$('body').on('click', '.delete_message', function (e) {
    e.preventDefault();

    var message_uuid = $(this).attr('data-email-UID');
    var folder = $(this).attr('data-email-folder');
    $('#busyloading').show();
    var formData = new FormData();
    formData.append('message_uuid', message_uuid);
    formData.append('folder', folder);
    formData.append('is_list', 0);
    formData.append('_token', CSRF_TOKEN);
    $.ajax({
        url: APP_URL + '/delete_email_by_uuid',
        type: 'POST',
        data: formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success: function (res) {
            var result = $.parseJSON(res);
            console.log(result);
            if (result.error == '') {

                $('.close_detail').trigger('click');
                $('.inner_msg_body_wrap .opened').remove();

                if (result.trashed == 1) {
                    success_msg('The email has been moved to the Trash.');
                } else {
                    success_msg('The email deleted successfully.');
                }
            } else if (result.error != '') {
                warning_msg(result.error);
            } else {
                alert_msg('Something went wrong. Please try again later or contact to administrator.');
            }
            $('#busyloading').hide();
            return false;
        }
    });
});

//Delete emails from list that are selected
$('body').on('click', '.delete_emails_from_list', function (e) {
    e.preventDefault();

    var checkedVals = $('.each_msg_check:checkbox:checked').map(function () {
        return this.value;
    }).get();
    //alert(checkedVals.join(","));

    var folder = $('.folders_wrap .active').attr('data-title');
    //alert(folder);

    $('#busyloading').show();
    var formData = new FormData();
    formData.append('message_uuid', checkedVals);
    formData.append('folder', folder);
    formData.append('is_list', 1);
    formData.append('_token', CSRF_TOKEN);
    $.ajax({
        url: APP_URL + '/delete_email_by_uuid',
        type: 'POST',
        data: formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success: function (res) {
            var result = $.parseJSON(res);
            console.log(result);
            if (result.error == '') {

                $(".inner_msg_body_wrap .each_msg_check").each(function () {
                    if ($(this).is(':checked')) {
                        $(this).parent().parent().parent().parent().parent().parent().remove();
                    }
                });
                $('.msg_action').hide();
                $('.close_detail').trigger('click');
                $('.all_msgs').prop('checked', false);

                if (result.trashed == 1) {
                    success_msg('The email(s) has been moved to the Trash.');
                } else {
                    success_msg('The email(s) deleted successfully.');
                }
            } else if (result.error != '') {
                warning_msg(result.error);
            } else {
                alert_msg('Something went wrong. Please try again later or contact to administrator.');
            }
            $('#busyloading').hide();
            return false;
        }
    });
});

//Move emails from one folder to another
$('body').on('click', '.move_emails', function (e) {
    e.preventDefault();

    var checkedVals = $('.each_msg_check:checkbox:checked').map(function () {
        return this.value;
    }).get();
    //alert(checkedVals.join(","));

    var folder = $(this).attr('folder-data');
    var from = $('.folders_wrap').find('.get_emails.active').attr('data-type');

    $('#busyloading').show();
    var formData = new FormData();
    formData.append('message_uuid', checkedVals);
    formData.append('from', from);
    formData.append('folder', folder);
    formData.append('is_list', 1);
    formData.append('_token', CSRF_TOKEN);
    $.ajax({
        url: APP_URL + '/move_emails_to_folder',
        type: 'POST',
        data: formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success: function (res) {
            var result = $.parseJSON(res);
            console.log(result);
            if (result.error == '') {

                $(".inner_msg_body_wrap .each_msg_check").each(function () {
                    if ($(this).is(':checked')) {
                        $(this).parent().parent().parent().parent().parent().parent().remove();
                    }
                });
                $('.msg_action').hide();
                $('.close_detail').trigger('click');
                $('.all_msgs').prop('checked', false);

                if (result.moved == 1) {
                    success_msg('The email(s) has been moved to the ' + folder + ' successfully.');
                }
            } else if (result.error != '') {
                warning_msg(result.error);
            } else {
                alert_msg('Something went wrong. Please try again later or contact to administrator.');
            }
            $('#busyloading').hide();
            return false;
        }
    });
});

//Download as zip file of attachment
$('body').on('click', '.download_attac', function () {
    var email_uid = $(this).attr('data-uuid');
    var email_from = $(this).attr('data-from');
    //$('#busyloading').show();
    var formData = new FormData();
    formData.append('email_uid', email_uid);
    formData.append('email_from', email_from);
  
    $.ajax({
        url: get_zip,
        type: 'POST',
        data: formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success: function (res) {
            console.log(res);
            //window.location.href();
            $('#busyloading').hide();
            window.open(res.data, '_blank');
            /*var result = $.parseJSON(res);
            if(result.success == 1){
                var file = result.file;
            }else{
                alert_msg('Something went wrong. Please try again later or contact to administrator.');
            }*/
            $('#busyloading').hide();
        }
    });
});

// email list scroll bar to load more emails
$('.msg_body_wrap').scroll(function () {
    var results = $(".inner_msg_body_wrap");

    if ($(this).scrollTop() + $(this).height() == results.height()) {
        if ($('.emails_loaded').val() < $('.total_emails').val()) {
            var limit = $('.email_list_limit').val();
            var folder = $('.email_folder').val();
            var from = $('.emails_loaded').val();
            var to = eval(from) + eval(limit);

            $('#busyloading').show();
            var formData = new FormData();
            formData.append('folder', folder);
            formData.append('from', from);
            formData.append('to', to);
            formData.append('_token', CSRF_TOKEN);
            $.ajax({
                url: APP_URL + '/get_email_load_more',
                type: 'POST',
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (res) {
                    var result = $.parseJSON(res);

                    if (result.error == '') {
                        if ((result.emails).length > 0) {
                            $('.emails_loaded').val(to);
                            $.each(result.emails, function (i, v) {
                                var clone = $('.message_clone').clone();
                                clone.removeClass('message_clone').removeClass('hide_important');

                                clone.find('.each_msg_check').attr('id', 'each_msg_' + v.uid);
                                clone.find('.check_label').attr('for', 'each_msg_' + v.uid);

                                if ($.inArray("seen", v['flags']) !== -1) {
                                    clone.find('.envelope > i').removeClass('fa-envelope').addClass('fa-envelope-open-o');
                                } else if ($.inArray("unseen", v['flags'])) {
                                    clone.find('.envelope > i').addClass('fa-envelope').removeClass('fa-envelope-open-o');
                                    clone.addClass('un_read');

                                }


                                if (v.attachment == true) {
                                    clone.find('.message_subject').html('<i class="fa fa-paperclip"></i>');
                                } else {
                                    clone.find('.message_subject').html('');
                                }
                                clone.find('.message_subject').append(v.subject);
                                clone.find('.email_date').html(v.date);
                                clone.find('.open_message').attr('data-msg_uuid', v.uid);
                                clone.find('.open_message').attr('data-folder', v.mailbox);

                                clone.find('.message_from').html();
                                if (v['from']['name'] !== null) {
                                    clone.find('.message_from').html(v['from']['name'] + ' <' + v['from']['email'] + '>');
                                } else {
                                    clone.find('.message_from').html(v['from']['email']);
                                }
                                $('.inner_msg_body_wrap').append(clone);
                            });
                        }
                    } else if (result.error != '') {
                        warning_msg(result.error);
                    } else {
                        alert_msg('Something went wrong. Please try again later or contact to administrator.');
                    }
                    $('#busyloading').hide();
                    return false;
                }
            });
        }
    }
});


// For design script

$(function () {
    var content_height = $('.page-content').height();

    $('.message_center').height(content_height);
    $('.left_menu').height(content_height - 10);
    $('.menu_body .msg_body_wrap').height(content_height - 40);
    $('.msg_body_detail').height(content_height + 20);
    $('.msg_view_wrap').height(content_height - 120);

});

$('body').on('click', '.each_folder', function () {
    $('.each_folder').removeClass('active');
    $(this).addClass('active');
});

$('body').on('click', '.drop_menu_arrow', function () {
    $('.drop_menu').slideToggle('slow');
});

$(window).click(function () {
    if ($('.drop_menu:visible').length != 0) {
        $('.drop_menu').slideToggle('slow');
    }
});

$('body').on('click', '.drop_menu_arrow', function (event) {
    event.stopPropagation();
});

$(document).on("change", "#all_msgs", function () {
    if (this.checked) {
        $('input[type=checkbox]').each(function () {
            $(this).parent().addClass("checked");
            $(this).prop('checked', true);
        });
        $('.msg_action').css('display', 'inline-block');
    } else {
        $('input[type=checkbox]').each(function () {
            $(this).parent().removeClass("checked");
            $(this).prop('checked', false);
        });
        $('.msg_action').css('display', 'none');
    }
});

$('.to_label').on('click', function () {
    $('.emails_to').focus();
});

$('body').on('click', '.each_msg_check', function () {
    if ($(this).hasClass('checked') === false) {
        $(this).addClass("checked");
        $(this).find('input[type="checkbox"]').prop('checked', true);
        // $('.msg_action').css('display', 'inline-block');
    } else {
        $(this).removeClass("checked");
        $(this).find('input[type="checkbox"]').prop('checked', false);
        // $('.msg_action').css('display', 'none');
    }

    var selected_check = $(".inner_msg_body_wrap").find('input[type="checkbox"]:checked').length;
    if (selected_check > 0) {
        $('.msg_action').css('display', 'inline-block');
    } else {
        $('.msg_action').css('display', 'none');
    }
});

$('body').on('click', '.middle_msg_section', function () {
    /*$('.msg_body_detail').fadeIn(500);*/
});
$('body').on('click', '.close_detail', function () {
    $('.msg_body_detail').fadeOut(100);
    $('.email_section.menu_body').removeClass('col-md-4').addClass('col-md-10');
    $('.msg_body_detail').removeClass('col-md-6').addClass('col-md-10');
});


$('body').on('click', '.compose_email', function (e) {
// $(function (e) {
    
    e.preventDefault();

    var quick_text = $(this).text();
    
    var object_name = $('.middle_overview .object_name:first').text();

    $('#new_mail .pop_heading').html('New Email');

    $('.note-editable').empty();

    var type = $(this).attr('type');
    var message = $(this).attr('message');
    var title = $(this).attr('title');

    var uuid = $('.uuid').val();
    // console.log(uuid);
    if (typeof type != 'undefined') {

        // For pipeline
        if (typeof uuid == 'undefined') {
            uuid = $(this).attr('uuid');
        }

        $('#newMail').find('.view_type').remove();
        $('#newMail').append('<input type="hidden" class="view_type" name="view_type" value="' + type + '"><input type="hidden" class="view_type" name="uuid" value="' + uuid + '">');
    }


    var content_height = $('#new_mail').height();

    var summer_note_height = content_height - 500;

    // console.log(summer_note_height);

    $('.email_content').height(summer_note_height);
    $('.new_attachments').css('min-height', 20);
    $('.new_attachments').css('max-height', 230);

    $('#busyloading').show();

    $('.emails_to').val('');
    $('.cc_input').val('');
    $('.bcc_input').val('');
    $('.subject').val('');
    $('.email_body').text('').val('');
    $('.filelists ol li').remove();
    


    if (quick_text == 'Escalate') {
        var code = $('.contact_type .yellow_btn:first').text();

        $('.subject').val('Escalate :::: ' + type + ' ' + code);
    }
    // $('#new_mail .note-editable p').empty().append('<br>');
    console.log(quick_text);
   

    // if (typeof object_name != 'undefined') {
    //     object_name = $.trim(object_name);
    //     $('.subject').val(object_name);
    // }

    

    $('.summernote').summernote({'height': summer_note_height});
    $('#busyloading').hide();

    $('.max-size').remove();


    $('#new_mail').modal();
    if (quick_text == 'Escalate Task') {
        
        var code = $(this).attr('code');
        $('.subject').val('Escalate :::: ' + type+ ' ' + code+ '-'+title);
       
        $('.note-editable p').empty().append(message);
    }

});

//Forward email
$('body').on('click', '.forward_message', function () {
// $(function (e) {

    if (EMAIL_USER_EMAIL == '' || EMAIL_USER_PASSWORD == '' || EMAIL_OUTGOING_HOST == '' || EMAIL_OUTGOING_PORT == '') {
        alert_msg('You are not configured your email settings please check your email settings before send an email.');
        return false;
    }

    $('#new_mail .pop_heading').html('Forwarding Email');

    var content_height = $('#new_mail').height();

    var summer_note_height = content_height - 500;

    // console.log(summer_note_height);

    $('.email_content').height(summer_note_height);
    $('.new_attachments').height(230);

    $('#busyloading').show();

    $('.emails_to').val('');
    $('.cc_input').val('');
    $('.bcc_input').val('');
    $('.email_body').text('').val('');
    var str1 = $('.msg_body_detail .subject').text();
    var str2 = "Fwd:";
    var subject = '';
    if (str1.indexOf(str2) != -1) {
        //console.log(str2 + " found");
        subject = str1;
    } else {
        subject = str2 + ' ' + str1;
    }
    $('.subject').val(subject);

    /*=======================*/
    var html_message = "<br><br><p>============ Forwarded message ============";
    html_message = html_message + "<br>From : " + $('.msg_view .message_from').html();
    html_message = html_message + "<br>" + $('.msg_view .msg_to').html();
    html_message = html_message + "<br>Date : " + $('.msg_view .date_time').html();
    html_message = html_message + "<br>Subject : " + str1;
    html_message = html_message + "<br>============ Forwarded message ============</p>";
    /*=======================*/

    html_message = html_message + '<br><br>' + $('.msg_body_detail .each_msg_detail').html();

    //console.log(html_message);
    //$('.email_body').text(html_message);

    $('.filelists ol li').remove();

    // $('#new_mail .note-editable p').empty().append('<br>');

    $('.summernote').summernote({'height': summer_note_height});
    $('.summernote').summernote('code', html_message);
    // $('.summernote').summernote({'height': summer_note_height});
    // $('.note-editable').html(html_message);
    $('#busyloading').hide();

    $('.max-size').remove();

    $('#new_mail').modal();

    //$('.each_attachment')
    var files = $('.each_attachment .attachment_img').map(function () {
        return $(this).attr('src');
    }).get();
    if (files.length > 1) {
        var formData = new FormData();
        formData.append('file_srcs', files);
        formData.append('_token', CSRF_TOKEN);
        $.ajax({
            url: APP_URL + '/attach_files',
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (res) {
                var result = $.parseJSON(res);
                console.log(result);
                if (result.total > 0) {


                    $.each(result.attachment, function (i, v) {
                        $('.filelist.complete').append('<li data-index="' + i + '"><span class="extension_img"><img src="http://localhost/sites/gbs-crm/public/global/img/png_img.png"></span><div class="image_upload_wrap"><span class="content"><span class="file">' + v.name + '</span> <span class="file_size">' + v.size + '</span><span class="cancel cancel_img"><i class="fa fa-times"></i></span></span><span class="bar" style="width: 100%;"></span><span class="progress">100% done</span></div><input class="file_names" name="file_name[]" value="' + v.name + '" type="hidden"></li>');
                    })
                }
            }
        });
    }
});

//Reply email
$('body').on('click', '.reply_message', function () {
// $(function (e) {

    if (EMAIL_USER_EMAIL == '' || EMAIL_USER_PASSWORD == '' || EMAIL_OUTGOING_HOST == '' || EMAIL_OUTGOING_PORT == '') {
        alert_msg('You are not configured your email settings please check your email settings before send an email.');
        return false;
    }

    $('#new_mail .pop_heading').html('Replying Email');

    var content_height = $('#new_mail').height();

    var summer_note_height = content_height - 500;

    // console.log(summer_note_height);

    $('.email_content').height(summer_note_height);
    $('.new_attachments').height(230);

    $('#busyloading').show();

    $('.emails_to').val($('.msg_view .message_from_email').html());
    $('.cc_input').val('');
    $('.bcc_input').val('');
    $('.email_body').text('').val('');
    var str1 = $('.msg_body_detail .subject').text();
    var str2 = "Re:";
    var subject = '';
    if (str1.indexOf(str2) != -1) {
        //console.log(str2 + " found");
        subject = str1;
    } else {
        subject = str2 + ' ' + str1;
    }
    $('.subject').val(subject);

    /*=======================*/
    /*var html_message = "<br><br><p>============ Forwarded message ============";
    html_message = html_message+"<br>From : "+$('.msg_view .message_from').html();
    html_message = html_message+"<br>"+$('.msg_view .msg_to').html();
    html_message = html_message+"<br>Date : "+$('.msg_view .date_time').html();
    html_message = html_message+"<br>Subject : "+str1;
    html_message = html_message+"<br>============ Forwarded message ============</p>";*/
    /*=======================*/

    var html_message = '<br><br><blockquote class="gmail_quote" style="margin:0 0 0 .8ex;border-left:1px #ccc solid;padding-left:1ex">' + $('.msg_body_detail .each_msg_detail').html() + '</blockquote>';

    //console.log(html_message);
    //$('.email_body').text(html_message);

    $('.filelists ol li').remove();

    // $('#new_mail .note-editable p').empty().append('<br>');

    $('.summernote').summernote({'height': summer_note_height});
    $('.summernote').summernote('code', html_message);
    // $('.summernote').summernote({'height': summer_note_height});
    // $('.note-editable').html(html_message);
    $('#busyloading').hide();

    $('.max-size').remove();

    $('#new_mail').modal();
});

$('.cc').click(function () {
    $('.cc_row').slideToggle('fast');
});

$('.bcc').click(function () {
    $('.bcc_row').slideToggle('fast');
});




// Linked with script


$('body').on('click', '.remove_linked', function () {
    $(this).parent().remove();
});

$('body').on('input', '.link_email .linked_with', function () {

    var search_string = $(this).val();

    if (search_string != '') {

        $('.filtered_data').empty().append('<i class="fa fa-spinner fa-spin" style="text-align: center; vertical-align: middle"></i>').show();
        $.ajax({
            url: APP_URL + '/search_objects',
            type: 'POST',
            data: {
                '_token': CSRF_TOKEN,
                search_string: search_string
            },
            success: function (res) {
                var result = $.parseJSON(res);
                if (result.length > 0) {
                    $('.filtered_data').empty();
                    var options = '';
                    $.each(result, function (i, v) {
                        var name_split = v.display_name.split(' ');
                        var first = name_split[0].charAt(0);
                        var last = '';
                        if (name_split.length > 1) {
                            last = name_split[1].charAt(0);
                        }
                        var bg_class = '#add2a1';
                        var bg_clr = '#add2a1';
                        var text = first + last;
                        if (v.type == 'Opportunity') {
                            bg_clr = '#5fb257';
                            text = 'O';
                        }
                        if (v.type == 'Lead') {
                            bg_clr = '#e89361';
                            text = 'L';
                        }
                        if (v.type == 'Account') {
                            bg_clr = '#86a6d4';
                            text = 'A';
                        }
                        if (v.type == 'Contact') {
                            bg_clr = '#898989';
                            text = 'C';
                        }


                        options += ('<div class="tag" uuid="' + v.uuid + '" display_name="' + v.display_name + '" type="' + v.type + '"><span style="background:' + bg_clr + '" >' + text + '</span>' + v.code + ' - ' + v.display_name + ' (' + v.type + ')</div>');

                    });

                    $('.filtered_data').empty().append(options);

                } else {
                    $('.filtered_data').empty().append('<div>No Results Found.</div>').show();
                }
            }
        });

    } else {
        $('.filtered_data').hide();
    }

});

// Select an tag from filtered objects

$('body').on('click', '.filtered_data .tag', function () {

    var uuid = $(this).attr('uuid');
    var display_name = $(this).attr('display_name');
    var type = $(this).attr('type');


    $('.linked_with_added').append('<span class="linked_with_tags" uuid="' + uuid + '" type="' + type + '">' + display_name + '<span class="remove_linked">×</span><input type="hidden" name="linked_with_tags[]" class="linked_with_tags_field" value="' + uuid + '^' + type + '^' + display_name + '"></span>');

    $('.link_email .linked_with').val('').trigger('');
    $('.filtered_data').hide();

});

// when linking btn press

$('body').on('click', '.linked_btn', function () {

    if ($('.linked_with_tags').length > 0) {
        $('#busyloading').show();

        var message_uid = $(this).attr('msg_uid');
        var subject = $('.msg_body_detail .subject').text();
        var message_from = $('.msg_body_detail .message_from').text();
        var msg_to = $('.msg_body_detail .msg_to').text();
        var msg_cc = $('.msg_body_detail .msg_cc').text();
        var msg_bcc = $('.msg_body_detail .msg_bcc').text();
        var each_msg_detail = $('.msg_body_detail .each_msg_detail').html();
        // var date_time = $('.msg_body_detail .date_time').text();


        var date_time = $(".msg_body_detail .date_time")
            .clone()    //clone the element
            .children() //select all the children
            .remove()   //remove all the children
            .end()  //again go back to selected element
            .text();

        msg_to = msg_to.replace("To : ", "");
        msg_cc = msg_cc.replace("CC : ", "");
        msg_bcc = msg_bcc.replace("BCC : ", "");


        var files = $('.msg_body_detail .each_attachment .attachment_img').map(function () {
            return $(this).attr('src');
        }).get();

        var linking_array = $('.msg_body_detail .linked_with_tags_field').map(function () {
            return $(this).val();
        }).get();


        var formData = new FormData();
        formData.append('file_srcs', files);
        formData.append('message_uid', message_uid);
        formData.append('subject', subject);
        formData.append('message_from', message_from);
        formData.append('msg_to', msg_to);
        formData.append('msg_cc', msg_cc);
        formData.append('msg_bcc', msg_bcc);
        formData.append('each_msg_detail', each_msg_detail);
        formData.append('linking_array', linking_array);
        formData.append('date_time', date_time);
        formData.append('_token', CSRF_TOKEN);

        $.ajax({
            url: APP_URL + '/linked_email_wd_obj',
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (res) {
                var result = $.parseJSON(res);
                if (result.error == 0 && result.exist_name == '') {
                    success_msg('Email linked successfully');

                } else if (result.error == 0 && result.exist_name != '') {
                    var names = result.exist_name.slice(0, -1);
                    warning_msg('Email already linked with ( ' + names + ' )');
                } else if (result.error == 1) {
                    alert_msg('Something went wrong please contact administrator');
                }

                $('.linked_with_tags').remove();
                $('#busyloading').hide();

            }
        });


    } else {
        swal('Alert!', 'Please search & select an object first.');
    }

});

// Loading template from drop to summernote

$('body').on('click', '.email_temp', function () {
    $('.filtered_data_email').slideToggle('slow');
});


$(window).click(function () {
    $('.filtered_data_email').slideUp();
});
$('body').on('click', '.email_temp', function (event) {
    event.stopPropagation();
});


$('body').on('click', '.each_template', function () {

    var uuid = $(this).attr('uuid');
    $('#busyloading').show();
    $.ajax({
        url: APP_URL + '/load_template',
        type: 'POST',
        data: {uuid: uuid, '_token': CSRF_TOKEN},
        success: function (res) {
            var result = $.parseJSON(res);
            result = "<br><p></p>" + result;

            $('.summernote').summernote('code', result);

            $('#busyloading').hide();

        }
    });

});