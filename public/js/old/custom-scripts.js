$(document).ready(function(){ 

$('.main-menu li .dropdown.drop-sml li a').click(function (e) { 
        e.preventDefault();
        var pageid = $(this).attr('href').split('#');
        var id = pageid[1];
        //$(".menu-"+id).addClass('active');
      pageid='#'+pageid[1];
        pos = $(pageid).offset();
        $('html, body').animate({
            scrollTop: pos.top - 80
        }, 800, function () {
            //window.location =pageid;
            history.pushState(null, null, pageid);
        });
        return false;
    });
    // 
    $(".flexslider.leftSlide").flexslider({
        animation: "slide",
        slideshow: !1,
        controlNav: !1,
        startAt: 0,
        touch: !1,
        after: function (t) {
            $(".serviceCtrlSlideA").addClass("selected")
        },
        before: function () {
            $(".serviceCtrlSlideA").removeClass("selected")
        },
        controlsContainer: $(".serviceCtrlSlideA")
    }), 
    $(".flexslider.rightSlide").flexslider({
        animation: "slide",
        controlNav: !1,
        slideshow: !1,
        startAt: 0,
        touch: !1,
        after: function (t) {
            $(".serviceCtrlSlideB").addClass("selected")
        },
        before: function () {
            $(".serviceCtrlSlideB").removeClass("selected")
        },
        controlsContainer: $(".serviceCtrlSlideB")
    }), 
    $(window).trigger("resize"); 
    $(".serviceCtrl .flex-nav-prev").on("click", function () {
    $(".serviceCtrlSlideB.selected .flex-next").trigger("click"), 
    $(".serviceCtrlSlideA.selected .flex-prev").trigger("click")
    }), 
    $(".serviceCtrl .flex-nav-next").on("click", function () {
    $(".serviceCtrlSlideB.selected .flex-prev").trigger("click"), 
    $(".serviceCtrlSlideA.selected .flex-next").trigger("click")
    }), 
    // 
    // home-slider
    $('.home-slider').bxSlider({
        auto: true
    });
    $('.testimonials').bxSlider({
        captions: true,
        auto: true
    });

    $(".open-panel").click(function () {
        $(".menu-toggle").slideToggle("fast");
    });
    // id-scroll
    $('.main-menu li').hover(function() {
        $(this).find('.dropdown').show();
    }, function() {
        $(this).find('.dropdown').hide();
    });
    $('.drop-lg a').hover(function() {
        $(this).find('.drop-img').show();
    }, function() {
        $(this).find('.drop-img').hide();
    });
    $("#scroll-area").niceScroll({
    cursorwidth:"6px",
    bouncescroll: true,
    touchbehavior:true
});
    $("#tabs").tabs();
    $("#trans-trade").tabs();
    $("#tab-address").tabs();
    
//Click event to scroll to top
    $('.scrolltop').click(function() {
        $('html, body').animate({
            scrollTop : 0
        }, 800);
        return false;
    });
    $('.clients').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });
   
    $('.sec-abt .box-left').each(function () {
        var imagePos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        if (imagePos < topOfWindow + 700) {
            $(this).addClass("slideRight");
        }
    
    }) ;
    $('.sec-abt .box-right').each(function () {
        var imagePos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        if (imagePos < topOfWindow + 700) {
            $(this).addClass("slideLeft");
        }
    
    }) ;
    
     $(function () {
        $("video")[0].play({});
    });
});

$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if ($(this).scrollTop() > 300) {
            $('.scrolltop').fadeIn();
        } else {
            $('.scrolltop').fadeOut();
    }
    if (scroll >= 50) {
        $("#header").addClass("bg_change");
    } else {
        $("#header").removeClass("bg_change");
    }

    $('.about-area .box-left, .widget-area .wid-1').each(function () {
        var imagePos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        if (imagePos < topOfWindow + 700) {
            $(this).addClass("slideRight");
        }
    });

    $('.about-area .box-right, .widget-area .wid-3').each(function () {
        var imagePos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        if (imagePos < topOfWindow + 700) {
            $(this).addClass("slideLeft");
        }
    
    }) ;

    $('.sec-head p, .sec-head h2,  #tabs .box-right,.ceo-msg .box-left h2, .ceo-msg .box-left p, .team-block .block-1,.team-block .block-2,.team-block .block-3,.team-block .block-4').each(function () {
        var imagePos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        if (imagePos < topOfWindow + 700) {
            $(this).addClass("slideUp");
        }
    
    }) ;
    $('#tabs .box-left').each(function () {
        var imagePos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        if (imagePos < topOfWindow + 700) {
            $(this).addClass("slideDown");
        }
    
    }) ;
    $('.contact .box-left, .contact .box-right,.widget-area .wid-2, .serve-block').each(function () {
        var imagePos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        if (imagePos < topOfWindow + 700) {
            $(this).addClass("fade");
        }
    
    }) ;
    
    
});

// slider-left-right

// slider-left-right

