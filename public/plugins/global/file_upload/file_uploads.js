/*
 |--------------------------------------------------------------------------
 | Files for contact, staff etc
 |--------------------------------------------------------------------------
 */

// $.getScript( url, function() {

    $('body').on('click', '.file_upload', function () {
        var size_upload = FILE_MAX_UPLOAD_SIZE / 1024;
        size_upload = size_upload / 1024;
        $('span.max-size').text('max file size ' + size_upload + ' mb');
        $('li.files a').trigger('click');
        // $('#files_form')[0].reset();
        $('#files_form .document_type').val('').trigger('change');
        $('ol.filelist').html('');
        // $('h5.completed, h5.uploading').hide();
        $('#files_form .title').val('');
        $('#files_form .reference').val('');
        $('#new_file').modal();
    })
    // var $ = jQuery.noConflict();
    
    Formstone.Ready(function () {
        $(".upload").upload({
            maxSize: FILE_MAX_UPLOAD_SIZE,//1073741824,
            // beforeSend: onBeforeSend,
            /*chunked: true*/
        }).on("start.upload", onStart)
            .on("complete.upload", onComplete)
            .on("filestart.upload", onFileStart)
            .on("fileprogress.upload", onFileProgress)
            // .on("filecomplete.upload", onFileComplete)
            .on("fileerror.upload", onFileError)
            .on("queued.upload", onQueued);
    
        $(".filelist.queue").on("click", ".cancel", onCancel);
        $(".cancel_all").on("click", onCancelAll);
    });
    // });
    // var $ = jQuery.noConflict();
    
    function onCancel(e) {
        console.log("Cancel");
        var index = $(this).parents("li").data("index");
        $(this).parents("form").find(".upload").upload("abort", parseInt(index, 10));
    }
    
    function onCancelAll(e) {
        console.log("Cancel All");
        $(this).parents("ol").find(".upload").upload("abort");
    }
    
    function onBeforeSendOld(formData, file) {
        // $('.filelist.complete').hide();
        // $(".completed").hide();
        console.log(file);
        // Cancel request
        /*if (file.name.indexOf(".jpg") < 0) {
            return false;
        }*/
        $(".uploading").show();
        console.log("Before Send");
        formData.append("document", file);
        formData.append("test_field", "test_value");
        formData.append("_token", CSRF_TOKEN);
        // return (file.name.indexOf(".jpg") < -1) ? false : formData; // cancel all jpgs
        return formData;
    }
    
    function onQueued(e, files) {
        console.log("Queued");
        var html = '';
    
        for (var i = 0; i < files.length; i++) {
    
            var iSize = (files[i].size / 1024);
            iSize = (iSize / 1024);
            iSize = (Math.round(iSize * 100) / 100);
    
            var split_name = files[i].name.split('.');
            var splitted_length = split_name.length;
            var find_ext_index = splitted_length - 1;
            var extension = split_name[find_ext_index];
    
            var base_path = "{{ asset('uploads) }}"
            html += '<li data-index="' + files[i].index + '"><span class="extension_img"><img src="public/uploads' + '"></span><div class="image_upload_wrap"><span class="content"><span class="file">' + files[i].name + '</span> <span class="file_size">' + iSize + ' Mb</span><span class="cancel cancel_img"><i class="fa fa-times"></i></span></span><span class="bar"></span><span class="progress">Queued</span></div></li>';
        }
    
        $(this).parents("form").find(".filelist.queue").append(html);
    }
    
    function onStart(e, files) {
        console.log("Start");
        $(this).parents("form").find(".filelist.queue")
            .find("li")
            .find(".progress").text("Waiting");
    }
    
    function onComplete(e) {
        console.log("Complete");
        $(".completed").show();
    
        var clone = $(this).parents("form").find('.filelist.queue').html();
        $(this).parents("form").find('.filelist.queue').empty();
        $('.filelist.complete').prepend(clone).show();
    
        $(this).parents("form").find(".uploading").hide();
        // All done!
    }
    
    function onFileStart(e, file) {
        console.log("File Start");
        $(this).parents("form").find(".filelist.queue")
            .find("li[data-index=" + file.index + "]")
            .find(".progress").text("0%");
    }
    
    function onFileProgress(e, file, percent) {
        console.log("File Progress" + ' ' + percent);
        var $file = $(this).parents("form").find(".filelist.queue").find("li[data-index=" + file.index + "]");
    
        $file.find(".progress").text(percent + "%" + ' done');
        $file.find(".bar").css("width", percent + "%");
    }
    
    function onFileComplete(e, file, response) {
        console.log("File Complete");
    
        if (response.trim() === "" || response.toLowerCase().indexOf("error") > -1) {
            $(this).parents("form").find(".filelist.queue")
                .find("li[data-index=" + file.index + "]").addClass("error")
                .find(".progress").text(response.trim()).height('auto');
        } else {
            var $target = $(this).parents("form").find(".filelist.queue").find("li[data-index=" + file.index + "]");
            $target.find(".file").text(file.name);
            $target.append('<input type="hidden" class="file_names" name="file_name[]" value="' + response + '" />');
            // $target.find(".completed").show();
            // $target.find(".uploading").hide();
            $target.appendTo($(this).parents("form").find(".filelist.queue"));
        }
    }
    
    function onFileError(e, file, error) {
        console.log("File Error");
        $(this).parents("form").find(".filelist.queue")
            .find("li[data-index=" + file.index + "]").addClass("error")
            .find(".progress").text("Error: " + error);
        $(this).parents("form").find(".filelist.queue").find("li[data-index=" + file.index + "]").append('<input type="hidden" class="file_error" name="file_error[]" value="1" />');
    }
    
    
    $('body').on('click', '.cancel_img', function (e) {
    
        $(this).parent().parent().parent().remove();
        var count_img = $('.filelist.complete li').length;
    
        if (count_img == 0) {
            $(".completed").hide();
        }
    });