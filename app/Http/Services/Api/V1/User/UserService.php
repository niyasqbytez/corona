<?php

namespace App\Http\Services\Api\V1\User;

use App\Http\Controllers\ApiController;
use Auth;
use Validator;
use Illuminate\Support\Facades\Config;
use App\Models\User;
use Hash;

class UserService extends ApiController
{

    /**
     * User Login
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function login($request) {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return $this->errorResponse(trans('api.error_required_fields'), $errorMessage);
        }
        $email = $request->email;
        $password = $request->password;
        // $user = User::where('email', $email)->where('password',$request->password)
        if (Auth::guard('api')->attempt(['email' => $email, 'password' => $password])) {
            $user = Auth::guard('api')->user();

            $data['user'] = $user;

            return $this->successResponse(trans('api.login_success'), $data);
        } else {
            return $this->errorResponse(trans('api.invalid_credentials'));
        }
    }

    /**
     * User Register
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function register($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone_code' => 'required',
            'mobile' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return $this->errorResponse(trans('api.error_required_fields'), $errorMessage);
        }

        $user_exist = User::where('mobile', '=', $request->mobile)
                ->orWhere('email', '=', $request->email)->first();
        if ($user_exist) {
            if ($request->mobile == $user_exist->mobile) {
                return $this->errorResponse(trans('api.mobile_exists'));
            }
            if ($request->email == $user_exist->email) {
                return $this->errorResponse(trans('api.email_exists'));
            }
            return $this->errorResponse(trans('api.user_exists'));
        }

        $user = new User;
        $user->name = $request->name;
        $user->phone_code = $request->phone_code;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->api_token = generateApiToken();
        $user->save();

        $data['user'] = $user;

        return $this->successResponse(trans('api.user_register_success'), $data);
    }
}
