<?php

namespace App\Http\Controllers\Api\V1\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Services\Api\V1\User\UserService;

class UserController extends Controller
{
    private $user_service;

    /**
     * User Service Construct
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function __construct(UserService $user_service) {

          $this->user_service= $user_service;
    }

    /**
     * User Login
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function login(Request $request) {
        return $this->user_service->login($request);
    }

    /**
     * User Register
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function register(Request $request)
    {
        return $this->user_service->register($request);
    }
}
