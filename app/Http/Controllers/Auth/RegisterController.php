<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Language;
use App\Models\UserTranslation;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function showRegistrationForm() {
        return view('auth.register');
    }
    public function register(Request $request) {
      
        $this->validate($request, [
            'name' => 'required|min:3|max:32|regex:/^[a-zA-Z ]*$/',
            'phone' => 'required|numeric',
            'email' => 'required|regex:/^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i|unique:users',
            'password' => 'required|same:password_confirmation',
            'chck2' => 'required',
        ],[
            "name.regex" => "Name only contain alphabets and spaces",
            "email.regex" => "Enter valid email",
            "chck2.required" => "Terms and conditions are required",
        ]);
        $advertiser = new User();
 
        $advertiser->email = $request->email;
        $advertiser->work_phone = $request->phone;
        $advertiser->password = $request->password;
        $advertiser->save();
        $languages = Language::where('status', 1)->get();
        foreach($languages as $language) {
            $translation = new UserTranslation();
            $translation->user_id = $advertiser->id;
            $translation->language_id = $language->id;
            $translation->display_name = $request->name;
            $translation->save();
        }
        return redirect(app()->getLocale().'/home');
    }
}
