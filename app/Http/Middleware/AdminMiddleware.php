<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        if ($request->ajax() && !Auth::guard('admin')->check()) {
            return response()->json(["status" => "expired"], 500);
        }
        if (Auth::guard('admin')->check()) {
            // return $next($request);
            $response = $next($request);
            return $response->header('Cache-Control','nocache, no-store, max-age=0, must-revalidate')
                    ->header('Pragma','no-cache')
                    ->header('Expires','Sat, 26 Jul 1997 05:00:00 GMT');
            
        }

        return redirect('/admin/login')->with('growl', ['Your don\'t have permission to acces this page.','danger']);
    }

}
