<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->lang) {
            if (app()->getLocale() != $request->lang) {
                App::setLocale($request->lang, 'en');
            }
        }
        if ($request->ajax() && !Auth::guard('web')->check()) {
            return response()->json(["sessionOut" => "your session out.Please Login"], 500);
        }

        if (Auth::guard('web')->check()) {
            App::setLocale($request->lang, 'en');
            return $next($request);
        }

        // return redirect()->with('growl', ['Your don\'t have permission to acces this page.', 'danger']);
        return redirect(app()->getLocale())->with('growl', ['Your don\'t have permission to acces this page.', 'danger']);
    }
}
