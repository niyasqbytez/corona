<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotWebApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = '')
    {
        $api = new ApiController();
        if ($request->hasHeader('Authorization')) {
            $token = $request->bearerToken();
            // $language = $api->setApiLanguage($request->header('language', 'en'));
            if ($token != env('API_TOKEN')) {
                return $api->errorResponse(trans('api.token_error'));
            }
        } else {
            return $api->errorResponse(trans('api.api_validate'));
        }
        return $next($request);
    }
}
