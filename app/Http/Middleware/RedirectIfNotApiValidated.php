<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotApiValidated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $api = new ApiController();
        if (!Auth::guard('api')->check()) {
            if ($request->ajax() || $request->wantsJson() || $guard == "api") {
                return $api->errorResponse(trans('api.unauthorized'));
            }
        }
        if (Auth::guard('api')->check()) {
            $language = $api->setApiLanguage($request->header('language', 'en'));
            return $next($request);
        }

        return $api->errorResponse(trans('api.user_not_exists'));
    }
}
