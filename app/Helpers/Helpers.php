<?php
// Get Current Time
function getCurrentTime($dts, $zone) {
    $date = new DateTime($dts, new DateTimeZone('UTC'));
    $date->setTimezone(new DateTimeZone($zone));
    return $date->format('Y-m-d H:i:s');
}

//Generate API token
function generateApiToken()
{
    do {
        $number = str_random(60);
    } while (!empty(\App\Models\User::where('api_token', $number)->first(['api_token'])));

    return $number;
}