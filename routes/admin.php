<?php

/*
  |--------------------------------------------------------------------------
  | Admin Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register Admin routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::group(['namespace' => 'Auth', 'as' => 'admin.'], function () {
    Route::get('/admin', function () {
        return redirect('admin/login');
    });
    Route::get('/login', 'AdminLoginController@showLoginForm')->name('login');
    Route::post('/login', 'AdminLoginController@login')->name('login.submit');
    Route::post('/logout', 'AdminLoginController@logout')->name('logout');

    //admin password reset routes
    Route::post('/password/email', 'AdminForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset', 'AdminForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/reset', 'AdminResetPasswordController@reset')->name('password.reset.post');
    Route::get('/password/reset/{token}', 'AdminResetPasswordController@showResetForm')->name('password.reset');

});

Route::group(['middleware' => 'isAdmin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {

    Route::get('/', 'HomeController@index')->name('dashboard');
    
});
