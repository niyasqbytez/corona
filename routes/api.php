<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v1', 'middleware' => 'webApi'], function () {

    Route::post('login','Api\V1\User\UserController@login');
    Route::post('register', 'Api\V1\User\UserController@register');
  
  });
  
  Route::group(['prefix' => '/v1', 'middleware' => 'isApiUser', 'namespace' => 'Api\V1\User', 'as' => 'api.'], function () {
    
  
  });
  